from django.contrib import admin
from .models import Shortener


@admin.register(Shortener)
class ShortenerModel(admin.ModelAdmin):
    list_display = ['shorthand', 'added_on', 'last_accessed']
