from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.db import transaction, models
import django_filters

from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.generics import RetrieveAPIView, ListCreateAPIView
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework import status, viewsets, permissions, mixins
from django_filters import rest_framework as filters
from drf_spectacular.utils import extend_schema, OpenApiParameter

from .serializers import ImportTestSuiteRunSerializer, IssueFilterSerializer, RunConfigSerializer, TextStatusSerializer
from .serializers import BuildSerializer, TestSerializer, BugTrackerAccountSerializer, KnownIssuesSerializer
from .serializers import serialize_MetricPassRatePerRunconfig, serialize_MetricPassRatePerTest, BugTrackerSerializer
from .serializers import BugCompleteSerializer, ShortenerSerializer, RestViewMachineSerializer, ComponentSerializer
from .serializers import ImportMachineError, ImportMachineSerializer, RunConfigDiffSerializer, RestIssueSerializer
from .serializers import UnknownFailureSerializer
from .filtering import QueryCreator

from .models import Component, Build, Test, Machine, RunConfigTag, RunConfig, TextStatus, Bug, TestResult
from .models import IssueFilter, Issue, KnownFailure, MachineTag, BugTrackerAccount, BugTracker, UnknownFailure
from .metrics import MetricPassRatePerRunconfig, MetricPassRatePerTest

from shortener.models import Shortener

import json
import re


def get_obj_by_id_or_name(model, key):
    try:
        return model.objects.get(pk=int(key))
    except ValueError:
        pass

    return get_object_or_404(model, name=key)


def object_vet(model, pk):
    obj = get_object_or_404(model, pk=pk)
    if not obj.vetted:
        obj.vet()
    return Response(status=status.HTTP_200_OK)


def object_suppress(model, pk):
    obj = get_object_or_404(model, pk=pk)
    if obj.vetted:
        obj.suppress()
    return Response(status=status.HTTP_200_OK)


class CustomPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = None

    def get_page_size(self, request):
        if self.page_size_query_param:
            try:
                page_size = int(request.query_params.get(self.page_size_query_param, self.page_size))
            except ValueError:
                return self.page_size

            # Clamp the maximum size
            if self.max_page_size:
                page_size = min(page_size, self.max_page_size)

            # Do not set any limits if the page size is null or negative
            if page_size <= 0:
                return None
            else:
                return page_size

        return self.page_size


class IssueViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Issue.objects.all().order_by('-id')
    serializer_class = RestIssueSerializer

    def patch(self, request, pk):
        issue = Issue.objects.get(pk=pk)
        serializer = RestIssueSerializer(issue, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @classmethod
    def _execute_action(cls, request, pk, permsission_name, action):
        if not request.user.has_perm(permsission_name):
            return Response(data={"message": f"User {request.user} doesn't have sufficient permissions"},
                            status=status.HTTP_401_UNAUTHORIZED)
        try:
            issue = get_object_or_404(Issue, pk=pk)
            action(issue)
        except Exception as err:
            return Response(data={"message": str(err)}, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)

    @action(detail=True)
    def archive(self, request, pk=None):
        return self._execute_action(request, pk, "CIResults.archive_issue", lambda issue: issue.archive(request.user))

    @action(detail=True)
    def restore(self, request, pk=None):
        return self._execute_action(request, pk, "CIResults.restore_issue", lambda issue: issue.restore())


class IssueFilterViewSet(viewsets.ModelViewSet):
    queryset = IssueFilter.objects.all().order_by('-id').prefetch_related('tags', 'tests__testsuite',
                                                                          'tests__first_runconfig',
                                                                          'machine_tags', 'machines',
                                                                          'statuses__testsuite')
    serializer_class = IssueFilterSerializer
    pagination_class = CustomPagination

    def __check_list__(self, request_data, field, field_name, db_class, errors):
        objects = set(request_data.get(field, []))

        objects_db = dict()
        for obj in db_class.objects.filter(id__in=objects):
            objects_db[obj.id] = obj

        if len(objects) != len(objects_db):
            errors.append("At least one {} does not exist".format(field_name))

        return objects, objects_db

    def __get_or_None__(self, klass, field, request_dict, errors):
        obj = None
        if field in request_dict:
            obj_id = request_dict[field]

            # Do not consider empty strings as meaning a valid value
            if isinstance(obj_id, str) and len(obj_id) == 0:
                return None

            # Convert the id to an int or fail
            try:
                obj_id = int(obj_id)
            except Exception:
                errors.append("The field '{}' needs to be an integer".format(field))
                return None

            # Try getting the object
            obj = klass.objects.filter(id=obj_id).first()
            if obj is None:
                errors.append("The object referenced by '{}' does not exist".format(field))

        return obj

    def get_queryset(self):
        queryset = self.queryset
        if description := self.request.query_params.get("description"):
            queryset = queryset.filter(description__contains=description)
        return queryset

    @transaction.atomic
    def create(self, request):
        errors = []
        if len(request.data.get('description', '')) == 0:
            errors.append("The field 'description' cannot be empty")

        # Check if the filter should replace another one
        edit_filter = self.__get_or_None__(IssueFilter, 'edit_filter',
                                           request.data, errors)
        edit_issue = self.__get_or_None__(Issue, 'edit_issue',
                                          request.data, errors)

        # Check that all the tags, machines, tests, and statuses are present
        tags, tags_db = self.__check_list__(request.data, "tags", "tag", RunConfigTag, errors)
        machine_tags, machine_tags_db = self.__check_list__(request.data, "machine_tags", "machine tag",
                                                            MachineTag, errors)
        machines, machines_db = self.__check_list__(request.data, "machines", "machine", Machine, errors)
        tests, tests_db = self.__check_list__(request.data, "tests", "test", Test, errors)
        statuses, statuses_db = self.__check_list__(request.data, "statuses", "status", TextStatus, errors)

        # Check the regular expressions
        for field in ['stdout_regex', 'stderr_regex', 'dmesg_regex']:
            try:
                re.compile(request.data.get(field, ""), re.DOTALL)
            except Exception:
                errors.append("The field '{}' does not contain a valid regular expression".format(field))

        # Create the object or fail depending on whether we got errors or not
        if len(errors) == 0:
            filter = IssueFilter.objects.create(description=request.data.get('description'),
                                                stdout_regex=request.data.get('stdout_regex', ""),
                                                stderr_regex=request.data.get('stderr_regex', ""),
                                                dmesg_regex=request.data.get('dmesg_regex', ""),
                                                user_query=request.data.get('user_query', ""))

            filter.tags.add(*tags_db)
            filter.machines.add(*machines_db)
            filter.machine_tags.add(*machine_tags_db)
            filter.tests.add(*tests_db)
            filter.statuses.add(*statuses_db)

            # If this filter is supposed to replace another filter
            if edit_filter is not None:
                if edit_issue is not None:
                    edit_issue.replace_filter(edit_filter, filter, request.user)
                else:
                    edit_filter.replace(filter, request.user)

            serializer = IssueFilterSerializer(filter)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)


class RunConfigFilter(filters.FilterSet):
    class Meta:
        model = RunConfig
        fields = {
            "name": ["exact", "contains"],
            "builds__name": ["exact", "contains"],
        }


class RunConfigViewSet(mixins.CreateModelMixin,
                       mixins.ListModelMixin,
                       mixins.RetrieveModelMixin,
                       viewsets.GenericViewSet):
    queryset = RunConfig.objects.all().order_by('-id')
    serializer_class = RunConfigSerializer
    lookup_value_regex = r"[\w.-]+"
    filterset_class = RunConfigFilter

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="id",
                description="A unique ID or name identifying this runconfig.",
                location=OpenApiParameter.PATH,
            )
        ]
    )
    def retrieve(self, request, pk=None):
        runconfig = self._get_runcfg(pk)
        serializer = RunConfigSerializer(runconfig)
        return Response(serializer.data)

    @classmethod
    def known_failures_serialized(cls, runcfg):
        f = KnownFailure.objects.filter(result__ts_run__runconfig=runcfg)
        failures = f.prefetch_related('result__status', 'result__test', 'result__test__testsuite',
                                      'result__ts_run__machine', 'matched_ifa__issue__bugs',
                                      'matched_ifa__issue__bugs__tracker')
        return KnownIssuesSerializer(failures, read_only=True, many=True)

    def _get_runcfg(self, obj_id):
        return get_obj_by_id_or_name(RunConfig, obj_id)

    @action(detail=True)
    def known_failures(self, request, pk=None):
        runcfg = self._get_runcfg(pk)
        return Response(self.known_failures_serialized(runcfg).data, status=status.HTTP_200_OK)

    @extend_schema(
        description="Compare two runconfigs",
        parameters=[
            OpenApiParameter(
                name="id",
                description="Name or ID of of the RunConfig used for the test suite run",
                location=OpenApiParameter.PATH
            ),
            OpenApiParameter(
                name="to",
                description="Id or name of RunConfig to compare",
                required=True
            ),
            OpenApiParameter(
                name="no_compress",
                description="Should not compress comparison results",
                type=bool
            ),
            OpenApiParameter(
                name="summary",
                description="Return raw text summary (to enable add this parameter to request)",
            )
        ],
        responses={200: RunConfigDiffSerializer}
    )
    @action(detail=True)
    def compare(self, request, pk=None):
        runcfg_from = self._get_runcfg(pk)
        runcfg_to = self._get_runcfg(request.GET.get('to'))
        no_compress = request.GET.get('no_compress') is not None

        diff = runcfg_from.compare(runcfg_to, no_compress=no_compress)
        if request.GET.get('summary') is None:
            serializer = RunConfigDiffSerializer(diff)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return HttpResponse(diff.text)

    @extend_schema(
        description=(
            "Create test suite run object"
        ),
        parameters=[
            OpenApiParameter(
                name="id",
                description="Id or name of base RunConfig",
                location=OpenApiParameter.PATH
            ),
        ],
        request=ImportTestSuiteRunSerializer,
        responses={201: None},
    )
    @action(detail=True, methods=["post"], url_path='testsuiterun')
    def import_test_suite_run(self, request, pk=None):
        try:
            request.data["runconfig_name"] = self._get_runcfg(pk).name
            serializer = ImportTestSuiteRunSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(status=status.HTTP_200_OK)
            else:
                return Response({"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response({"message": str(error)}, status=status.HTTP_400_BAD_REQUEST)


class ComponentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Component.objects.all().order_by('-id')
    serializer_class = ComponentSerializer


class BuildViewSet(mixins.CreateModelMixin,
                   mixins.ListModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   viewsets.GenericViewSet):
    queryset = Build.objects.all().order_by('-id')
    serializer_class = BuildSerializer

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="id",
                description="A unique ID or name identifying this build.",
                location=OpenApiParameter.PATH,
            )
        ]
    )
    def retrieve(self, request, pk=None):
        build = get_obj_by_id_or_name(Build, pk)
        serializer = BuildSerializer(build)
        return Response(serializer.data)


class MachineViewSet(viewsets.ViewSet, mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Machine.objects.all()
    serializer_class = RestViewMachineSerializer
    filterset_fields = {
        "id": ["exact"],
        "name": ["exact"],
        "public": ["exact"],
        "vetted_on": ["isnull"],
    }

    def create(self, request, *args, **kwargs):
        serializer = ImportMachineSerializer(data=request.data)
        try:
            if serializer.is_valid():
                machine = serializer.save()
                return Response(
                    {"status": "success", "data": RestViewMachineSerializer(machine).data},
                    status=status.HTTP_201_CREATED
                )
            return Response({"status": "error", "message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
        except ImportMachineError as err:
            return Response({"status": "error", "message": str(err)}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=["post"])
    def vet(self, request, pk):
        return object_vet(Machine, pk)

    @action(detail=True, methods=["post"])
    def suppress(self, request, pk):
        return object_suppress(Machine, pk)


class TestFilter(filters.FilterSet):
    class Meta:
        model = Test
        fields = {
            'id': ['exact'],
            'name': ['exact'],
            'testsuite': ['exact'],
            'public': ['exact'],
            'added_on': ['lte', 'gte'],
            'vetted_on': ['isnull'],
        }
        filter_overrides = {
            models.DateTimeField: {
                'filter_class': django_filters.IsoDateTimeFilter
            },
        }


class TestSet(viewsets.ReadOnlyModelViewSet):
    queryset = Test.objects.all().order_by('-id')
    serializer_class = TestSerializer
    filterset_class = TestFilter

    @action(detail=True, methods=["post"])
    def vet(self, request, pk):
        return object_vet(Test, pk)

    @action(detail=True, methods=["post"])
    def suppress(self, request, pk):
        return object_suppress(Test, pk)


class UnknownFailureViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = UnknownFailure.objects.all().order_by('-id')
    serializer_class = UnknownFailureSerializer
    pagination_class = CustomPagination

    EXTRA_FIELDS_ARG = OpenApiParameter(
        name="extra_fields",
        description=(
            f"Comma-separated list of fields to expand (available choices: "
            f"{', '.join(serializer_class.extra_fields())})"
        ),
        type=str,
    )

    @extend_schema(description="Retrieve a single unknown failure", parameters=[EXTRA_FIELDS_ARG])
    def retrieve(self, request, pk):
        extra_fields = request.query_params.get('extra_fields', '').split(',')
        failure = get_object_or_404(UnknownFailure, pk=pk)
        serializer = self.serializer_class(failure, extra_fields=extra_fields)
        return Response(serializer.data)

    @extend_schema(description="List all unknown failures", parameters=[EXTRA_FIELDS_ARG])
    def list(self, request):
        page = self.paginate_queryset(self.queryset)
        extra_fields = request.query_params.get('extra_fields', '').split(',')
        serializer = self.serializer_class(page, extra_fields=extra_fields, many=True)
        return self.get_paginated_response(serializer.data)


class TextStatusViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = TextStatus.objects.all().order_by('-id')
    serializer_class = TextStatusSerializer

    @action(detail=True)
    def vet(self, request, pk):
        return object_vet(TextStatus, pk)

    @action(detail=True)
    def suppress(self, request, pk):
        return object_suppress(TextStatus, pk)


class BugTrackerViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = BugTracker.objects.all().order_by('-id')
    serializer_class = BugTrackerSerializer


class BugViewSet(RetrieveAPIView):
    serializer_class = BugCompleteSerializer
    queryset = Bug.objects.all()

    def _get_bugtracker(self):
        tracker = self.kwargs.get('tracker')

        try:
            return BugTracker.objects.get(pk=int(tracker))
        except (ValueError, BugTracker.DoesNotExist):
            pass

        try:
            return BugTracker.objects.get(name=tracker)
        except BugTracker.DoesNotExist:
            pass

        return get_object_or_404(BugTracker, short_name=tracker)

    def retrieve(self, request, *args, **kwargs):
        bug = get_object_or_404(Bug, tracker=self._get_bugtracker(), bug_id=kwargs.get('bug_id'))
        serializer = self.get_serializer(bug)
        return Response(serializer.data)


class BugTrackerAccountViewSet(viewsets.ModelViewSet):
    # WARNING: we do not yet perform access control for setting who is a user or developer because of the limited damage
    # this can cause and the annoyance of having to ask large group of users to authenticate then be granted the
    # privilege to change the roles
    permission_classes = []
    authentication_classes = []

    queryset = BugTrackerAccount.objects.all().order_by('-id')
    serializer_class = BugTrackerAccountSerializer

    http_method_names = ['get', 'patch']


class ShortenerViewSet(ListCreateAPIView, viewsets.GenericViewSet):
    # WARNING: No access control is performed because these objects can be created simply by navigating the website
    permission_classes = []
    authentication_classes = []

    queryset = Shortener.objects.all().order_by('-id')
    serializer_class = ShortenerSerializer

    def create(self, request, *args, **kwargs):
        if request.method != "POST" or request.content_type != "application/json":
            raise ValueError("Only JSON POST requests are supported")

        data = json.loads(request.body)
        fulls = data.get('full')
        if fulls is None:
            raise ValueError("Missing the field 'full' which should contain the full text to be shortened")

        if isinstance(fulls, list):
            shorts = [Shortener.get_or_create(full=f) for f in fulls]
            serializer = self.get_serializer(shorts, many=True)
        else:
            short = Shortener.get_or_create(full=fulls)
            serializer = self.get_serializer(short)

        return JsonResponse(serializer.data, safe=False)


@api_view()
@permission_classes((permissions.AllowAny,))
def metrics_passrate_per_runconfig_view(request):
    user_query = QueryCreator(request, TestResult).request_to_query()
    history = MetricPassRatePerRunconfig(user_query)
    return Response(serialize_MetricPassRatePerRunconfig(history))


@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def metrics_passrate_per_test_view(request):
    user_query = QueryCreator(request, TestResult).request_to_query()
    passrate = MetricPassRatePerTest(user_query)
    return Response(serialize_MetricPassRatePerTest(passrate))
