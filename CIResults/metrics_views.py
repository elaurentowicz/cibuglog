from django.shortcuts import render
from django.http import JsonResponse

from .models import Bug, Test, Machine, RunConfig, Issue, UnknownFailure, BugComment

from .metrics import metrics_issues_over_time, metrics_bugs_over_time, metrics_issues_ttr, metrics_bugs_ttr
from .metrics import metrics_open_issues_age, metrics_open_bugs_age, metrics_failure_filing_delay
from .metrics import MetricPassRatePerRunconfig, MetricRuntimeHistory, Periodizer, metrics_comments_over_time
from .metrics import MetricPassRatePerTest

from .filtering import QueryCreator


def api_metrics(request):
    metrics = {
        "issues": Issue.objects.filter(archived_on=None).count(),
        "machine": Machine.objects.all().count(),
        "suppressed_tests": Test.objects.filter(vetted_on=None).exclude(first_runconfig=None).count(),
        "suppressed_machines": Machine.objects.filter(vetted_on=None).count(),
        "tests": Test.objects.all().count(),
        "unknown_failures": UnknownFailure.objects.filter(result__ts_run__runconfig__temporary=False).count(),
        "version": 1,
    }

    return JsonResponse(metrics)


def metrics_generic_history(request, History, history_kwargs, template_path, **kwargs):
    # Default to a sane query if no query has been made
    runconfigs = RunConfig.objects.filter(temporary=False).order_by('-id')[:3]
    runconfigs_str = ','.join(["'{}'".format(r.name) for r in runconfigs])
    default_query = "runconfig_name IS IN [{}]".format(runconfigs_str)

    user_query = QueryCreator(request, History.filtering_model,
                              default_query_parameters={'query': default_query}).request_to_query()

    context = {
        'history': History(user_query, **history_kwargs),
    }
    return render(request, template_path, context)


def metrics_passrate_per_runconfig(request, **kwargs):
    return metrics_generic_history(request, MetricPassRatePerRunconfig, {},
                                   'CIResults/metrics_passrate_runconfig.html', **kwargs)


def metrics_passrate_per_test(request, **kwargs):
    return metrics_generic_history(request, MetricPassRatePerTest, {},
                                   'CIResults/metrics_passrate_test.html', **kwargs)


def metrics_runtime_history(request, **kwargs):
    return metrics_generic_history(request, MetricRuntimeHistory,
                                   {"average_per_machine": request.GET.get('average', '0') == '1'},
                                   'CIResults/metrics_runtime.html', **kwargs)


def metrics_issues(request, **kwargs):
    period = request.GET.get("period", None)
    periodizer = None if period is None else Periodizer.from_json(period)

    user_query = QueryCreator(request, Issue).request_to_query()

    context = {
            "issues_weekly":  metrics_issues_over_time(user_query, periodizer=periodizer).stats,
            "issues_ttr": metrics_issues_ttr().stats,
            "issues_age": metrics_open_issues_age().stats,
            "failure_filing_delay": metrics_failure_filing_delay().stats,

            'filters_model': Issue,
            'query': user_query,
        }
    return render(request, 'CIResults/metrics_issues.html', context)


def metrics_bugs(request, **kwargs):
    period = request.GET.get("period", None)
    periodizer = None if period is None else Periodizer.from_json(period)

    user_query = QueryCreator(request, Bug).request_to_query()

    bugs_trend, comments_trend = metrics_bugs_over_time(user_query, periodizer=periodizer)
    context = {
            "bugs_trend":  bugs_trend.stats,
            "comments_trend":  comments_trend.stats,
            "bugs_ttr": metrics_bugs_ttr(user_query).stats,
            "bugs_age": metrics_open_bugs_age(user_query).stats,

            'filters_model': Bug,
            'query': user_query,
        }
    return render(request, 'CIResults/metrics_bugs.html', context)


def metrics_comments(request, **kwargs):
    period = request.GET.get("period", None)
    periodizer = None if period is None else Periodizer.from_json(period)

    user_query = QueryCreator(request, BugComment).request_to_query()

    comments_trend, per_account_periods = metrics_comments_over_time(user_query, periodizer=periodizer)
    context = {
            "verbose": request.GET.get("verbose", None) is not None,
            "comments_trend":  comments_trend.stats,
            "per_account_periods": per_account_periods,

            'filters_model': BugComment,
            'query': user_query,
        }
    return render(request, 'CIResults/metrics_comments.html', context)
