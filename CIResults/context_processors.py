import os


class CIBugLog:
    def __init__(self, request):
        self.request = request

    @property
    def version(self):
        return os.getenv('CIBUGLOG_VERSION', None)

    @property
    def project_url(self):
        return "https://gitlab.freedesktop.org/gfx-ci/cibuglog"

    @property
    def version_url(self):
        return "{}/commit/{}".format(self.project_url, self.version)

    @property
    def admin_email(self):
        return os.getenv('ADMIN_EMAIL', None)


def global_context(request):
    return {
        "cibuglog": CIBugLog(request),
    }
