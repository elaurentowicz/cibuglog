import operator
import re
import traceback
from collections.abc import Callable
from typing import Any

import pytz
from arpeggio import EOF, NoMatch, OneOrMore, Optional, ParserPython, PTNodeVisitor
from arpeggio import RegExMatch as _
from arpeggio import ZeroOrMore, visit_parse_tree
from dateutil import parser as datetimeparser
from django.contrib import messages
from django.db.models import ForeignKey, ManyToManyField, Q
from django.http.request import QueryDict
from django.utils import timezone
from django.utils.dateparse import parse_duration
from django.utils.functional import cached_property

from shortener.models import Shortener


# Arpeggio's parser
def val_none(): return _(r'NONE')


def val_int(): return _(r'-?\d+')


def val_str(): return [('"', _(r'([^"\\]|\\.)*'), '"'),
                       ("'", _(r"([^'\\]|\\.)*"), "'")]


def val_bool(): return [r'TRUE', r'FALSE']


def val_datetime(): return 'datetime(', [_(r'[^\)]+')], ')'


def val_duration(): return 'duration(', [_(r'[^\)]+')], ')'


def val_ago(): return 'ago(', [_(r'[^\)]+')], ')'


def val_array(): return "[", OneOrMore(([val_none, val_str, val_int, val_bool, val_datetime, val_duration, val_ago],
                                        Optional(','))), "]"


def nested_expression(): return ZeroOrMore(ZeroOrMore(_(r'[^()]+')), ZeroOrMore("(", nested_expression, ")"),
                                           ZeroOrMore(_(r'[^()]+')))


def val_subquery(): return [('(', nested_expression, ')')]


def filter_field(): return _(r'[a-zA-Z\d_-]+')


def filter_object(): return _(r'\w+'), Optional(".", filter_field)


def basic_filter(): return [(filter_object, ['IS IN', 'NOT IN'], val_array),
                            (filter_object, ['<=', r'<', r'>=', r'>'], [val_int, val_datetime,
                                                                        val_duration, val_ago]),
                            (filter_object, ['=', '!='], [val_duration, val_datetime, val_int,
                                                          val_bool, val_str, val_none]),
                            (filter_object, [r'~=', r'MATCHES', r'ICONTAINS'], val_str),
                            (filter_object, [r'CONTAINS'], [val_str, val_array]),
                            (filter_object, [r'MATCHES'], val_subquery)]


def orderby_object(): return _(r'-?\w+')


def orderby(): return ("ORDER_BY", orderby_object)


def limit(): return ("LIMIT", val_int)


def factor(): return Optional("NOT"), [basic_filter, ("(", expression, ")")]


def expression(): return factor, ZeroOrMore(["AND", "OR"], factor), Optional(orderby), Optional(limit)


def query(): return Optional(expression), EOF


class QueryVisitor(PTNodeVisitor):
    class NoneObject:
        pass

    def __init__(self, model, ignore_fields=[], *arg, **kwargs):
        """
        Args:
            ignore_fields (list): List of fields whose filter conditions will be ignored during parsing.
        """
        self.model = model
        self.orderby = None
        self.limit = None
        self.ignore_db_paths = []
        for field in ignore_fields:
            if obj := self.model.filter_objects_to_db.get(field, {}):
                self.ignore_db_paths.append(obj.db_path)

        super().__init__(*arg, **kwargs)

    def is_m2m(self, path: str) -> bool:
        model = self.model
        for field_name in path.split("__"):
            if model._meta.get_field(field_name).__class__ is ManyToManyField:
                return True
            if model._meta.get_field(field_name).__class__ is ForeignKey:
                model = getattr(model, field_name).field.remote_field.model
                continue
            break
        return False

    def visit_val_none(self, node, children):
        # HACK: I would have rather returned None, but Arppegio interprets this as
        # a <no match>... Instead, return a NoneObject that will later be converted
        return QueryVisitor.NoneObject()  # pragma: no cover

    def visit_val_int(self, node, children):
        return FilterObjectInteger.parse_value(node.value)

    def visit_val_str(self, node, children):
        if len(children) == 0:
            return ""
        if len(children) > 1:
            raise ValueError("val_str cannot have more than one child")  # pragma: no cover
        return FilterObjectStr.parse_value(children[0])

    def visit_val_bool(self, node, children):
        return FilterObjectBool.parse_value(node.value)

    def visit_val_datetime(self, node, children):
        if len(children) > 1:
            raise ValueError("val_datetime cannot have more than one child")  # pragma: no cover
        return FilterObjectDateTime.parse_value(children[0])

    def visit_val_duration(self, node, children):
        if len(children) > 1:
            raise ValueError("val_duration cannot have more than one child")  # pragma: no cover
        return FilterObjectDuration.parse_value(children[0])

    def visit_val_ago(self, node, children):
        if len(children) > 1:
            raise ValueError("val_ago cannot have more than one child")  # pragma: no cover
        duration = FilterObjectDuration.parse_value(children[0])
        return timezone.now() - duration

    def visit_filter_field(self, node, children):
        if '__' in node.value:
            raise ValueError("Dict object keys cannot contain the substring '__'")

        return node.value

    def visit_filter_object(self, node, children):
        filter_obj = self.model.filter_objects_to_db.get(children[0])
        if filter_obj is None:
            raise ValueError("The object '{}' does not exist".format(children[0]))

        if isinstance(filter_obj, FilterObjectJSON):
            if len(children) != 2:
                raise ValueError("The dict object '{}' requires a key to access its data".format(children[0]))
            filter_obj = FilterObjectJSON(filter_obj._db_path, filter_obj.description, children[1])
        elif len(children) != 1:
            raise ValueError("The object '{}' cannot have an associated key".format(children[0]))

        return filter_obj

    def visit_val_array(self, node, children):
        return [c for c in children if c != ',']

    def visit_val_subquery(self, node, children):
        out = ""
        for x in list(node):
            out += str(x.flat_str())
        return out

    def visit_basic_filter(self, node, children):
        if len(children) != 3:
            raise ValueError("basic_filter: Invalid amount of operands")    # pragma: no cover

        filter_obj, lookup, item = children

        if filter_obj.db_path in self.ignore_db_paths:
            return self.emit_empty()

        # HACK: see visit_val_none()
        if isinstance(item, QueryVisitor.NoneObject):
            item = None  # pragma: no cover

        obj = None
        if isinstance(filter_obj, FilterObjectModel):
            item = filter_obj.parse_value(item)
            obj = self.emit_is_in_operator(filter_obj, item)
        else:
            lookups_map = {
                "<=": self.emit_lte_operator,
                ">=": self.emit_gte_operator,
                "<": self.emit_lt_operator,
                ">": self.emit_gt_operator,
                "CONTAINS": (
                    self.emit_contains_list_operator if isinstance(item, list) else self.emit_contains_string_operator
                ),
                "ICONTAINS": self.emit_icontains_operator,
                "IS IN": self.emit_is_in_operator,
                "NOT IN": self.emit_is_in_operator,
                "MATCHES": self.emit_matches_operator,
                "~=": self.emit_matches_operator,
                "=": self.emit_equal_operator,
                "!=": self.emit_equal_operator,
            }
            if lookup not in lookups_map:
                raise ValueError("Unknown lookup '{}'".format(lookup))      # pragma: no cover
            obj = lookups_map[lookup](filter_obj, item)

        if lookup in ['!=', 'NOT IN']:
            return self.emit_not_operator(obj)
        else:
            return obj

    def visit_factor(self, node, children):
        if len(children) > 1:
            if children[0] == "NOT":
                return self.emit_not_operator(children[-1])
        return children[-1]

    def visit_orderby_object(self, node, children):
        reverse = node.value[0] == '-'

        obj_name = node.value if not reverse else node.value[1:]

        filter_obj = self.model.filter_objects_to_db.get(obj_name)
        if filter_obj is not None:
            return "{}{}".format("-" if reverse else "", filter_obj.db_path)
        else:
            raise ValueError("The object '{}' does not exist".format(obj_name))

    def visit_orderby(self, node, children):
        if len(children) == 1:
            self.orderby = children[0]
        else:
            raise ValueError("orderby: Invalid amount of operands")    # pragma: no cover

    def visit_limit(self, node, children):
        if len(children) == 1:
            if children[0] < 0:
                raise ValueError("Negative limits are not supported")

            self.limit = children[0]
        else:
            raise ValueError("limit: Invalid amount of operands")    # pragma: no cover

    def visit_expression(self, node, children):
        if len(children) >= 1:
            result = children[0]
            for i in range(2, len(children), 2):
                if children[i-1] == "AND":
                    result = self.emit_and_operator(result, children[i])
                elif children[i-1] == "OR":
                    result = self.emit_or_operator(result, children[i])
            return result

    def visit_query(self, node, children):
        if len(children) > 1:
            raise ValueError("query cannot have more than one child")  # pragma: no cover
        elif len(children) == 1:
            return children[0]
        else:
            return self.emit_empty()

    def emit_equal_operator(self, filter_obj, item):
        raise NotImplementedError()  # pragma: no cover

    def emit_lte_operator(self, filter_obj, item):
        raise NotImplementedError()  # pragma: no cover

    def emit_lt_operator(self, filter_obj, item):
        raise NotImplementedError()  # pragma: no cover

    def emit_gte_operator(self, filter_obj, item):
        raise NotImplementedError()  # pragma: no cover

    def emit_gt_operator(self, filter_obj, item):
        raise NotImplementedError()  # pragma: no cover

    def emit_contains_string_operator(self, filter_obj, item):
        raise NotImplementedError()  # pragma: no cover

    def emit_contains_list_operator(self, filter_obj, item):
        raise NotImplementedError()  # pragma: no cover

    def emit_icontains_operator(self, filter_obj, item):
        raise NotImplementedError()  # pragma: no cover

    def emit_is_in_operator(self, filter_obj, item):
        raise NotImplementedError()  # pragma: no cover

    def emit_matches_operator(self, filter_obj, item):
        raise NotImplementedError()  # pragma: no cover

    def emit_not_operator(self, x):
        raise NotImplementedError()  # pragma: no cover

    def emit_and_operator(self, x, y):
        raise NotImplementedError()  # pragma: no cover

    def emit_or_operator(self, x, y):
        raise NotImplementedError()  # pragma: no cover

    def emit_empty(self):
        raise NotImplementedError()  # pragma: no cover


class VisitorQ(QueryVisitor):
    def __init__(self, model, ignore_fields=[], *arg, **kwargs):
        super().__init__(model, ignore_fields=ignore_fields, *arg, **kwargs)

    def get_related_model(self, path: str):
        model = self.model
        for field_name in filter(None, path.split("__")):
            model = getattr(model, field_name).field.remote_field.model
        return model

    def process_compare_operator(self, filter_obj, item, condition_key):
        key = filter_obj.db_path
        # Intercept lookups on many-to-many fields to allow filtering by multiple values
        # (e.g. machine_tag = "A" AND machine_tag = "B")
        # NOTE: This is needed to make the Python and the Q parsers behave in the same way
        if self.is_m2m(filter_obj.db_path):
            # Key has to be split into two separate ones to filter by multiple nested values in relation
            # "many-to-many" (example: ts_run__machine__tags__name):
            # * "key": path to parent key that is queried over (based on example: ts_run__machine)
            # * "subquery_key": relative path to child value key (based on example: tags__name)
            key_parts = key.split("__")
            key = "__".join(key_parts[:-2])
            subquery_key = "__".join(key_parts[-2:]) + condition_key
            objects = self.get_related_model(key).objects
            if condition_key == "__exact" and isinstance(item, list):
                for value in item:
                    objects = objects.filter(Q(**{f"{subquery_key}": value}))
            else:
                objects = objects.filter(Q(**{f"{subquery_key}": item}))
            item = objects
            key = key or "pk"
            condition_key = "__in"

        return Q(**{key + condition_key: item})

    def emit_lte_operator(self, filter_obj, item):
        return self.process_compare_operator(filter_obj, item, "__lte")

    def emit_lt_operator(self, filter_obj, item):
        return self.process_compare_operator(filter_obj, item, "__lt")

    def emit_gte_operator(self, filter_obj, item):
        return self.process_compare_operator(filter_obj, item, "__gte")

    def emit_gt_operator(self, filter_obj, item):
        return self.process_compare_operator(filter_obj, item, "__gt")

    def emit_contains_string_operator(self, filter_obj, item):
        return self.process_compare_operator(filter_obj, item, "__contains")

    def emit_contains_list_operator(self, filter_obj, item):
        return self.process_compare_operator(filter_obj, item, "__exact")

    def emit_icontains_operator(self, filter_obj, item):
        return self.process_compare_operator(filter_obj, item, "__icontains")

    def emit_is_in_operator(self, filter_obj, item):
        return self.process_compare_operator(filter_obj, item, "__in")

    def emit_matches_operator(self, filter_obj, item):
        return self.process_compare_operator(filter_obj, item, "__regex")

    def emit_equal_operator(self, filter_obj, item):
        return self.process_compare_operator(filter_obj, item, "__exact")

    def emit_not_operator(self, x):
        return ~x

    def emit_and_operator(self, x, y):
        return x & y

    def emit_or_operator(self, x, y):
        return x | y

    def emit_empty(self):
        return Q()


def getnested(attr_path):
    def nested_getter(obj):
        for attr in attr_path.split('.'):
            try:
                obj = getattr(obj, attr)
            except AttributeError:
                obj = obj.get(attr)
                if obj is None:
                    return None
        return obj
    return nested_getter


def compose(f1, f2):
    return lambda *args, **kwargs: f1(f2(*args, **kwargs))


def create_and_op(f1, f2):
    return lambda x: f1(x) and f2(x)


def create_or_op(f1, f2):
    return lambda x: f1(x) or f2(x)


def function_compare_factory(fn, getter, value):
    return lambda x: fn(getter(x), value)


class VisitorLocal(QueryVisitor):
    def __init__(self, model, ignore_fields=[], *arg, **kwargs):
        super().__init__(model, ignore_fields=ignore_fields, *arg, **kwargs)

    @staticmethod
    def get_list_getter(field_name):
        related_field_name = field_name.split(".")[-1]
        field_name = ".".join(field_name.split(".")[:-1])
        getter = getnested(field_name)
        return lambda x: [getattr(value, related_field_name) for value in getter(x).all()]

    def parse_field_name(self, filter_obj):
        return filter_obj.db_path.replace("__", ".")

    def emit_lte_operator(self, filter_obj, item):
        return function_compare_factory(operator.le, getnested(self.parse_field_name(filter_obj)), item)

    def emit_lt_operator(self, filter_obj, item):
        return function_compare_factory(operator.lt, getnested(self.parse_field_name(filter_obj)), item)

    def emit_gte_operator(self, filter_obj, item):
        return function_compare_factory(operator.ge, getnested(self.parse_field_name(filter_obj)), item)

    def emit_gt_operator(self, filter_obj, item):
        return function_compare_factory(operator.gt, getnested(self.parse_field_name(filter_obj)), item)

    def emit_contains_string_operator(self, filter_obj, item):
        return function_compare_factory(operator.contains, getnested(self.parse_field_name(filter_obj)), item)

    def emit_contains_list_operator(self, filter_obj, item):
        return function_compare_factory(
            lambda x, y: set(y).issubset(set(x)), self.get_list_getter(self.parse_field_name(filter_obj)), item
        )

    def emit_icontains_operator(self, filter_obj, item):
        return function_compare_factory(
            lambda x, y: y.lower() in x.lower(), getnested(self.parse_field_name(filter_obj)), item
        )

    def emit_is_in_operator(self, filter_obj, item):
        if self.is_m2m(filter_obj.db_path):
            return function_compare_factory(
                lambda x, y: any([val in x for val in y]), self.get_list_getter(self.parse_field_name(filter_obj)), item
            )
        return function_compare_factory(lambda x, y: x in y, getnested(self.parse_field_name(filter_obj)), item)

    def emit_matches_operator(self, filter_obj, item):
        return function_compare_factory(
            lambda x, y: bool(re.search(y, x)), getnested(self.parse_field_name(filter_obj)), item
        )

    def emit_equal_operator(self, filter_obj, item):
        if self.is_m2m(filter_obj.db_path):
            return function_compare_factory(
                operator.contains, self.get_list_getter(self.parse_field_name(filter_obj)), item
            )
        return function_compare_factory(operator.eq, getnested(self.parse_field_name(filter_obj)), item)

    def emit_not_operator(self, x):
        return compose(operator.not_, x)

    def emit_and_operator(self, x, y):
        return create_and_op(x, y)

    def emit_or_operator(self, x, y):
        return create_or_op(x, y)

    def emit_empty(self):
        return lambda _: True


class QueryParserPython:
    def __init__(self, model, user_query, ignore_fields: list[str] = []):
        self.model = model
        self.user_query = user_query
        self.error = None
        self.matching_fn: Callable[[Any], bool]

        try:
            parser = ParserPython(query)
            parse_tree = parser.parse(user_query)
            query_visitor = VisitorLocal(model, ignore_fields=ignore_fields)
            self.matching_fn = visit_parse_tree(parse_tree, query_visitor)
        except (ValueError, NoMatch) as err:
            self.error = str(err)

    @property
    def is_valid(self):
        return self.error is None


class QueryParser:
    def __init__(self, model, user_query, ignore_fields: list[str] = []):
        self.model = model
        self.user_query = user_query

        self.error = None
        self.q_objects = Q()
        self.orderby = None
        self.limit = None

        try:
            parser = ParserPython(query)
            parse_tree = parser.parse(self.user_query)
            query_visitor = VisitorQ(self.model, ignore_fields=ignore_fields)

            self.q_objects = visit_parse_tree(parse_tree, query_visitor)
            self.orderby = query_visitor.orderby
            self.limit = query_visitor.limit
        except ValueError as e:
            self.error = str(e)
        except NoMatch as e:
            self.error = str(e)

    @property
    def query_key(self):
        return Shortener.get_or_create(self.user_query).shorthand

    @property
    def is_valid(self):
        return self.error is None

    @property
    def is_empty(self):
        return not self.is_valid or len(self.user_query) == 0

    @cached_property
    def objects(self):
        if self.is_valid:
            query = self.model.objects.filter(self.q_objects).distinct()
            query = query.order_by(self.orderby) if self.orderby is not None else query
            return query[:self.limit] if self.limit is not None else query
        else:
            return self.model.objects.none()


class LegacyParser:
    userfilters_allowed_lookups = {'exact': '=', 'in': 'IS IN', 'regex': '~=', 'contains': 'CONTAINS',
                                   'icontains': 'ICONTAINS', 'gt': '>', 'gte': '>=', 'lt': '<', 'lte': '<='}
    userfilters_allowed_types = ['str', 'int', 'bool', 'datetime', 'duration']

    def __init__(self, model, **user_filters):
        # Filters should all be of the following format:
        # (only|exclude)__(object)__(in|regex|gt|lt) = str or format(value)
        lookups = "|".join(self.userfilters_allowed_lookups.keys())
        format_re = re.compile((r'(?P<action>(only|exclude))__(?P<object>\w+)__'
                               '(?P<lookup>({lookups}))'.format(lookups=lookups)))

        # Iterate through the user filters, match them to our format regex,
        # then construct the right ORM call
        only = []
        exclude = []
        for key, item in user_filters.items():
            match = format_re.match(key)
            if match:
                fields = match.groupdict()

                db_object = model.filter_objects_to_db.get(fields['object'])
                if db_object is None:
                    continue

                # aggregate all regular expressions into one request
                if fields['lookup'] == 'regex' and isinstance(item, list) and len(item) > 1:
                    item = r'('+'|'.join(item)+')'

                # Try converting the item to the right unit
                item = self._convert_user_values(item)

                bfilter = "{} {} {}".format(fields['object'],
                                            self.userfilters_allowed_lookups.get(fields['lookup']),
                                            item)

                if fields['action'] == 'only':
                    only.append(bfilter)
                else:
                    exclude.append(bfilter)

        self.query = " AND ".join(only)
        if len(exclude) > 0:
            if len(only) > 0:
                self.query += ' AND '
            self.query += "NOT ({})".format(" AND ".join(exclude))

    @classmethod
    def _convert_user_value(cls, value):
        # Will automatically be cached by python
        types = "|".join(cls.userfilters_allowed_types)
        item_re = re.compile(r'(?P<type>({types}))\((?P<value>.*)\)'.format(types=types))

        match = item_re.match(value)
        if match:
            fields = match.groupdict()

            try:
                if fields['type'] == 'str':
                    return "'{}'".format(fields['value'])
                elif fields['type'] == 'bool':
                    return "TRUE" if FilterObjectBool.parse_value(fields['value']) else "FALSE"
                elif fields['type'] == 'int':
                    return fields['value']
                elif fields['type'] == 'datetime' or fields['type'] == 'duration':
                    return value
            except Exception:                 # pragma: no cover
                traceback.print_exc()         # pragma: no cover

        # Default to the variable being a string
        return "'" + value + "'"

    @classmethod
    def _convert_user_values(cls, items):
        # detect whether we have a singular value or a list
        if isinstance(items, list):
            if len(items) > 1:
                new = []
                for item in items:
                    new.append(cls._convert_user_value(item))
                return "[" + ", ".join(new) + "]"
            else:
                return cls._convert_user_value(items[0])
        else:
            return cls._convert_user_value(items)


class UserFiltrableMixin:
    @classmethod
    def _get_value_from_params(cls, user_filters, key):
        val = user_filters.get(key)
        if isinstance(val, list) and len(val) == 1:
            val = val[0]
        return val

    @classmethod
    def from_user_filters(cls, prefix=None, **user_filters):
        query_param_name = f'{prefix}_query' if prefix is not None else 'query'
        query = cls._get_value_from_params(user_filters, query_param_name)
        if query is None:
            query_key = cls._get_value_from_params(user_filters, f'{query_param_name}_key')
            short = Shortener.objects.filter(shorthand=query_key).first()
            if short is not None:
                query = short.full

        if query is not None:
            return QueryParser(cls, query)
        else:
            query = LegacyParser(cls, **user_filters).query
            return QueryParser(cls, query)


class FilterObject:
    def __init__(self, db_path, description=None):
        self._db_path = db_path
        self._description = description

    @property
    def db_path(self):
        return self._db_path

    @property
    def description(self):
        if self._description is None:
            return "<no description yet>"
        else:
            return self._description


class FilterObjectJSON(FilterObject):
    data_type = "anything"
    documentation = "Expected format: <JSON field>.<key>"
    test_value = "test"

    def __init__(self, db_path, description=None, key=None):
        self.key = key
        super().__init__(db_path, description)

    @property
    def db_path(self):
        if self.key is None:
            raise ValueError("Dict field require a key to be accessed")  # pragma: no cover
        return "{}__{}".format(self._db_path, self.key)


class FilterObjectStr(FilterObject):
    data_type = "string"
    documentation = "Expected format: anything. Use quotes for the new query language (\"\" or ''). " \
                    "Escape quotes by placing '\\' before quote character."
    test_value = "str_test"

    def __init__(self, db_path, description=None):
        super().__init__(db_path, description)

    @classmethod
    def parse_value(cls, value):
        return str(value)


class FilterObjectDateTime(FilterObject):
    data_type = "datetime"
    documentation = "Expected format: datetime(YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ])"
    test_value = "2019-01-01"

    def __init__(self, db_path, description=None):
        super().__init__(db_path, description)

    @classmethod
    def parse_value(cls, value):
        return timezone.make_aware(datetimeparser.parse(value), pytz.utc)


class FilterObjectDuration(FilterObject):
    data_type = "duration"
    documentation = 'Expected format: "duration(DD HH:MM:SS.uuuuuu)", or "duration(P4DT1H15M20S)" (ISO 8601), ' \
                    'or "duration(3 days 04:05:06)" (PostgreSQL).'
    test_value = "123.456 seconds"

    def __init__(self, db_path, description=None):
        super().__init__(db_path, description)

    @classmethod
    def parse_value(cls, value):
        duration = parse_duration(value)
        if duration is None:
            raise ValueError("The value '{}' does not represent a duration. {}".format(value, cls.documentation))
        return duration


class FilterObjectBool(FilterObject):
    data_type = "boolean"
    documentation = "Supported values: bool(false)/bool(0) or bool(true)/bool(1). " \
                    "Use TRUE or FALSE for the new query language."
    test_value = "True"

    def __init__(self, db_path, description=None):
        super().__init__(db_path, description)

    @classmethod
    def parse_value(cls, value):
        return str(value).lower() in ["1", "true"]


class FilterObjectInteger(FilterObject):
    data_type = "integer"
    documentation = "Supported values: int(12345). Use 12345 for the new query language."
    test_value = 12345

    def __init__(self, db_path, description=None):
        super().__init__(db_path, description)

    @classmethod
    def parse_value(cls, value):
        return int(float(value))


class FilterObjectModel(FilterObject):
    data_type = "subquery"
    documentation = "Expected format: Any query compatible with the model selected"

    def __init__(self, model, db_path, description=None):
        self.model = model
        super().__init__(db_path, description)

    def parse_value(self, value):
        result = QueryParser(self.model, value)
        if not result.is_valid:
            raise ValueError(result.error)
        return result.objects


class QueryCreator:
    def __init__(self, request, Model, prefix=None, default_query_parameters={}):
        self.request = request
        self.Model = Model
        self.prefix = prefix
        self.default_query_parameters = default_query_parameters

    def __create_query_from_filters(self, **requested_filters):
        query = self.Model.from_user_filters(self.prefix, **requested_filters)
        if len(query.user_query) > 0:
            if not query.is_valid and self.request:
                messages.error(self.request, "Filtering error: " + query.error)
            return query
        return None

    def __build_query_string(self):
        op_mappings = {
            'string': 'MATCHES',
            'datetime': 'MATCHES',
            'integer': 'MATCHES',
        }
        query_str = ""
        for obj in self.Model.filter_objects_to_db:
            param_value = self.request.GET.get(f'{self.prefix}_{obj}' if self.prefix else obj)
            if param_value:
                data_type = self.Model.filter_objects_to_db[obj].data_type
                if len(query_str) > 0:
                    query_str += " AND "
                query_str += f"{obj} {op_mappings[data_type]} '{param_value}'"
        return query_str

    def string_to_query(self, query_string):
        query_param_name = f'{self.prefix}_query' if self.prefix else 'query'
        query_dict = QueryDict('', mutable=True)
        query_dict.update({f'{query_param_name}': query_string})
        query = self.__create_query_from_filters(**query_dict)
        if query:
            return query
        return self.Model.from_user_filters(**self.default_query_parameters)

    def request_to_query(self):
        for params in [self.request.POST, self.request.GET]:
            # convert the user filters to a normal dictionary to prevent issues when
            # inserting new values
            requested_filters = params.copy()
            query = self.__create_query_from_filters(**requested_filters)
            if query:
                return query

        return self.Model.from_user_filters(**self.default_query_parameters)

    def multiple_request_params_to_query(self):
        query_str = self.__build_query_string()
        return self.string_to_query(query_str)
