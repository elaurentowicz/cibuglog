from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import ValidationError, PermissionDenied
from django.utils.safestring import mark_safe
from django.views import View
from django.views.generic import DetailView, ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView, FormView
from django.http import JsonResponse, HttpResponseRedirect
from django.db import transaction
from django.db.models import Prefetch
from django.urls import reverse
from django.utils.functional import cached_property
from django.core.paginator import Paginator

from collections import namedtuple

from .models import BugTracker, Bug, Component, Build, Test, Machine, RunConfigTag, RunConfig, TestSuite, TextStatus
from .models import TestResult, IssueFilter, IssueFilterAssociated, Issue, KnownFailure, UnknownFailure, MachineTag

from .metrics import metrics_testresult_statuses_stats, metrics_testresult_machines_stats
from .metrics import metrics_testresult_tests_stats, metrics_testresult_issues_stats
from .metrics import metrics_knownfailure_statuses_stats, metrics_knownfailure_machines_stats
from .metrics import metrics_knownfailure_tests_stats, metrics_knownfailure_issues_stats

from .forms import TestMassRenameForm

from .filtering import QueryCreator, QueryParser

import traceback
import markdown
import json
import re


def index(request, **kwargs):
    uf = UnknownFailure.objects.select_related("result").prefetch_related(
        "result__test",
        "result__test__testsuite",
        "result__ts_run",
        "result__ts_run__machine",
        "result__ts_run__machine__tags",
        "result__ts_run__runconfig",
        "result__ts_run__testsuite",
        "result__status__testsuite",
        "result__status",
        "matched_archived_ifas",
        "matched_archived_ifas__issue__bugs__tracker",
    )
    uf = uf.defer("result__dmesg", "result__stdout", "result__stderr")
    uf = uf.filter(result__ts_run__runconfig__temporary=False).order_by('id')

    ifas_query = IssueFilterAssociated.objects.filter(deleted_on=None).all()
    issues = Issue.objects.filter(archived_on=None,
                                  expected=False).prefetch_related(Prefetch('filters',
                                                                            to_attr='ifas_cached',
                                                                            queryset=ifas_query),
                                                                   "ifas_cached__filter",
                                                                   "ifas_cached__added_by",
                                                                   "ifas_cached__deleted_by",
                                                                   "bugs__assignee__person",
                                                                   "bugs__tracker",
                                                                   "bugs").order_by('-id')

    def get_page(object_query, page, page_size=100):
        paginator = SafePaginator(object_query, page_size)
        return paginator.page(page)

    query_get_param = request.GET.copy()

    suppressed_tests_query = QueryCreator(request, Test, prefix="suppressed_tests").multiple_request_params_to_query()
    suppressed_tests = suppressed_tests_query.objects.prefetch_related('testsuite').filter(vetted_on=None) \
        .exclude(first_runconfig=None).prefetch_related('first_runconfig')
    suppressed_tests_page = query_get_param.get('suppressed_tests', 1)

    suppressed_machines_query = QueryCreator(request, Machine,
                                             prefix="suppressed_machine_lst").multiple_request_params_to_query()
    suppressed_machines = suppressed_machines_query.objects.filter(vetted_on=None)
    suppressed_machines_page = query_get_param.get('suppressed_machines', 1)

    paginated_tables = {
        'suppressed_tests': {
            'table': suppressed_tests,
            'page': suppressed_tests_page,
        },
        'suppressed_machines': {
            'table': suppressed_machines,
            'page': suppressed_machines_page,
        }
    }
    for table_name in paginated_tables:
        if table_name == query_get_param.get('table') and (page := query_get_param.get('page')):
            paginated_tables[table_name]['page'] = page
        paginated_tables[table_name]['table'] = get_page(
                                                    paginated_tables[table_name]['table'],
                                                    paginated_tables[table_name]['page'],
                                                )
        query_get_param.setlist(table_name, [paginated_tables[table_name]['page']])

    suppressed_statuses_query = QueryCreator(request, TextStatus,
                                             prefix="suppressed_statuses_lst").multiple_request_params_to_query()
    suppressed_statuses = suppressed_statuses_query.objects.filter(vetted_on=None)

    context = {
            "trackers": BugTracker.objects.all(),
            "issues": issues,
            "suppressed_tests": paginated_tables['suppressed_tests']['table'],
            "suppressed_machines": paginated_tables['suppressed_machines']['table'],
            "suppressed_statuses": suppressed_statuses,
            "unknown_failures": uf,
            "query_get_param": query_get_param,
        }
    return render(request, 'CIResults/index.html', context)


class SafePaginator(Paginator):
    def validate_number(self, number):
        try:
            number = int(number)
        except ValueError:
            number = 1
        return min(number, self.num_pages)


class IssueListView(ListView):
    model = Issue

    def get_queryset(self):
        self.query = QueryCreator(self.request, self.model).request_to_query()

        # Prefetch all the necessary things before returning the list
        ifas_query = IssueFilterAssociated.objects.all()
        issues = self.query.objects.prefetch_related(Prefetch('filters',
                                                              to_attr='ifas_cached',
                                                              queryset=ifas_query),
                                                     "ifas_cached__filter",
                                                     "ifas_cached__added_by",
                                                     "ifas_cached__deleted_by",
                                                     "bugs__tracker",
                                                     "bugs")
        if self.query.orderby is None:
            issues = issues.order_by('-id')
        return issues

    def get_context_data(self):
        context = super().get_context_data()
        context["trackers"] = BugTracker.objects.all()
        context['issues'] = context.pop('object_list')
        context['filters_model'] = Issue
        context['query'] = self.query
        return context

    def post(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class IssueView(View):
    ParsedParams = namedtuple('ParsedParams',
                              'bugs bugs_form description error filters')

    def __parse_params__(self, params):
        # Keep track of all errors, only commit if no error occured
        error = False

        # Parse the bugs
        bugs = []
        bugs_form = []
        for param in params:
            if param.startswith("bug_id_"):
                # Check the bug_id
                bug_id = params[param].strip()
                if len(bug_id) == 0:
                    continue

                # Check the tracker
                suffix = param[7:]
                tracker_pk = params.get("bug_tracker_{}".format(suffix), -1)
                try:
                    tracker_pk = int(tracker_pk)
                    tracker = BugTracker.objects.get(pk=tracker_pk)
                    tracker_valid = True

                    # Check the bug
                    try:
                        bug = Bug.objects.get(tracker=tracker, bug_id=bug_id)
                        bugs.append(bug)
                        bug_valid = True
                    except Exception:
                        # The bug does not exist, try to create it
                        try:
                            bug = Bug(tracker=tracker, bug_id=bug_id)
                            bug.poll()
                            bugs.append(bug)
                            bug_valid = True
                        except Exception:
                            error = True
                            bug_valid = False
                            traceback.print_exc()
                except Exception:
                    error = True
                    tracker_valid = False
                    bug_valid = False
                    traceback.print_exc()

                # Keep the information about the bugs, in case we need to
                # use it again to re-draw the page
                bugs_form.append({"bug_id": bug_id, "tracker_id": tracker_pk,
                                  "bug_valid": bug_valid,
                                  "tracker_valid": tracker_valid})

        # Parse the filters
        filters = []
        for param in params:
            if param.startswith("filter_"):
                try:
                    pk = int(params[param])
                    filters.append(IssueFilter.objects.get(pk=pk))
                except Exception:
                    error = True
                    continue

        # Get the description
        description = params.get("description", "")

        return self.ParsedParams(bugs=bugs, bugs_form=bugs_form,
                                 description=description, error=error,
                                 filters=filters)

    def __fetch_from_db__(self, pk):
        issue = get_object_or_404(Issue, pk=pk)

        bugs = issue.bugs.all()
        bugs_form = []
        for bug in bugs:
            bugs_form.append({"bug_id": bug.bug_id, "tracker_id": bug.tracker.id,
                             "bug_valid": True, "tracker_valid": True})

        filters_assoc = IssueFilterAssociated.objects.filter(deleted_on=None, issue=issue)
        filters = [e.filter for e in filters_assoc]

        return self.ParsedParams(bugs=bugs, bugs_form=bugs_form,
                                 description=issue.description, error=False,
                                 filters=filters)

    @transaction.atomic
    def __save__(self, pk, d, user):
        if d.error:
            raise ValueError("The data provided contained error")

        if len(d.filters) == 0:
            raise ValueError("No filters found")

        if len(d.bugs) == 0:
            raise ValueError("No bugs found")

        # Try to find the issue to edit, if applicable
        try:
            issue = Issue.objects.get(id=pk)

            # Update the fields
            issue.description = d.description
            issue.save()
        except Issue.DoesNotExist:
            issue = Issue.objects.create(filer=user.email, added_by=user,
                                         description=d.description)

        # Add the bugs and filters that were set
        issue.set_bugs(d.bugs)
        issue.set_filters(d.filters, user)

    def __show_page__(self, request, pk, d):
        # Pre-allocate a line for the bug
        if len(d.bugs_form) == 0:
            d.bugs_form.append({"bug_id": "", "tracker_id": -1,
                                "bug_valid": True, "tracker_valid": True})

        # find out whether we are creating or editing an issue
        if pk is None:
            save_url = reverse('CIResults-issue', kwargs={"action": 'create'})
            issue_title = "New Issue"
            pk = ""
        else:
            save_url = reverse('CIResults-issue', kwargs={"action": "edit",
                                                          "pk": pk})
            issue_title = "Edit Issue {}".format(pk)

        filters = list(IssueFilter.objects.filter(hidden=False).order_by("-id")[:20])
        for f in d.filters:
            if f not in filters:
                filters.append(f)

        # Get the list of tests referenced by this issue, then add all the ones
        # that have an associated first runconfig
        tests = set()
        for f in d.filters:
            tests.update(f.tests.all())
        tests.update(Test.objects.prefetch_related("testsuite").exclude(first_runconfig=None))

        # Render the page
        context = {
            "issue_id": pk,
            "issue_title": issue_title,
            "tags": RunConfigTag.objects.all(),
            "machine_tags": MachineTag.objects.all(),
            "machines": Machine.objects.all(),
            "tests": tests,
            "statuses": TextStatus.objects.prefetch_related("testsuite").all(),
            "trackers": BugTracker.objects.all(),
            "filters": filters,
            "field_bugs": d.bugs_form,  # This data may not be valid or commited yet
            "field_filters": d.filters,
            "field_description": d.description,
            "save_url": save_url,
            "filters_model": UnknownFailure,
        }
        return render(request, 'CIResults/issue.html', context)

    def __edit_issue__(self, request, action, pk, params):
        if pk is not None and len(params) == 0:
            if not request.user.has_perm('CIResults.change_issue'):
                raise PermissionDenied()

            # We are editing an issue and have not provided updates for it
            d = self.__fetch_from_db__(pk)
            return self.__show_page__(request, pk, d)
        else:
            if not request.user.has_perm('CIResults.add_issue'):
                raise PermissionDenied()

            # In any other case, just display what we have, or default values
            d = self.__parse_params__(params)

            # Save, if possible. Otherwise, just show the page again
            try:
                self.__save__(pk, d, request.user)
                return redirect('CIResults-index')
            except ValueError:
                return self.__show_page__(request, pk, d)

    def __route_request__(self, request, kwargs, params):
        post_actions = ["archive", "restore", "hide", "show"]

        action = kwargs['action']
        if action in post_actions:
            if request.method != 'POST':
                raise ValidationError("The action '{}' is unsupported in a {} request".format(action, request.method))

            # Check permissions
            if not request.user.has_perm('CIResults.{}_issue'.format(action)):
                raise PermissionDenied()

            issue = get_object_or_404(Issue, pk=kwargs.get('pk'))

            if action == 'archive':
                issue.archive(request.user)
            else:
                getattr(issue, action)()

            return redirect(request.META.get('HTTP_REFERER', 'CIResults-index'))
        else:
            # Action == edit or create
            return self.__edit_issue__(request, action,
                                       kwargs.get('pk'), params)

    def get(self, request, *args, **kwargs):
        return self.__route_request__(request, kwargs, request.GET)

    def post(self, request, *args, **kwargs):
        return self.__route_request__(request, kwargs, request.POST)


class IssueFilterView(View):
    def __update_stats__(self, stats, result):
        for tag in result.ts_run.runconfig.tags.all():
            stats['tags'].add((tag.id, str(tag)))
        stats['machines'].add((result.ts_run.machine.id, str(result.ts_run.machine)))
        if len(result.ts_run.machine.tags_cached) == 0:
            stats['tagless_machines'].add((result.ts_run.machine.id, str(result.ts_run.machine)))
        else:
            for tag in result.ts_run.machine.tags_cached:
                stats['machine_tags'].add((tag.id, str(tag)))
        stats['tests'].add((result.test.id, str(result.test)))
        stats['statuses'].add((result.status.id, str(result.status)))

    def __check_regexp__(self, params, field, errors):
        try:
            regexp = params.get(field, "")
            re.compile(regexp, re.DOTALL)
            return True
        except re.error as e:
            errors.append({"field": field, "msg": str(e)})
            return False

    def __parse_filter_from_params__(self, params) -> IssueFilter:
        filter = IssueFilter(description=params.get('description'),
                             stdout_regex=params.get('stdout_regex', ""),
                             stderr_regex=params.get('stderr_regex', ""),
                             dmesg_regex=params.get('dmesg_regex', ""),
                             user_query=params.get('user_query', ""))
        filter.tags_ids_cached = set(params.get("tags", []))
        filter.__machines_cached__ = set(Machine.objects.filter(id__in=params.get("machines", [])))
        filter.__machine_tags_cached__ = set(MachineTag.objects.filter(id__in=params.get("machine_tags", [])))
        filter.tests_ids_cached = set(params.get("tests", []))
        filter.statuses_ids_cached = set(params.get("statuses", []))

        # Assign for purpose of converting to user query
        filter.tags_cached = set(RunConfigTag.objects.filter(id__in=params.get("tags", [])))
        filter.tests_cached = set(Test.objects.filter(id__in=params.get("tests", [])))
        filter.statuses_cached = set(TextStatus.objects.filter(id__in=params.get("statuses", [])))
        return filter

    def __stats__(self, request):
        stats = {
            "matched": {
                "tags": set(),
                "machine_tags": set(),
                "machines": set(),
                "tagless_machines": set(),
                "tests": set(),
                "statuses": set()
            },
            "errors": [
            ],
        }

        # Read the parameters and perform some input validation
        params = json.loads(request.body.decode())
        self.__check_regexp__(params, 'stdout_regex', stats['errors'])
        self.__check_regexp__(params, 'stderr_regex', stats['errors'])
        self.__check_regexp__(params, 'dmesg_regex', stats['errors'])

        filter = self.__parse_filter_from_params__(params)

        parser = QueryParser(UnknownFailure, filter.equivalent_user_query)
        if parser.error:
            stats['errors'].append({"field": "user_query", "msg": parser.error})

        # Check the filter
        if len(stats['errors']) == 0:
            filter = self.__parse_filter_from_params__(params)

            # Check which failures are matched by this filter
            matched_failures = (
                parser.objects.select_related("result")
                .prefetch_related(
                    "result__ts_run__runconfig__tags",
                    "result__ts_run__machine",
                    "result__ts_run__machine__tags",
                    "result__test",
                    "result__test__testsuite",
                    "result__status",
                )
                .filter(result__ts_run__runconfig__temporary=False)
                .defer("result__dmesg", "result__stdout", "result__stderr")
            )

            for failure in matched_failures:
                self.__update_stats__(stats['matched'], failure.result)

            for key in stats['matched']:
                stats['matched'][key] = sorted(stats['matched'][key])
            stats['matched']['failure_total'] = UnknownFailure.objects.filter(
                result__ts_run__runconfig__temporary=False
            ).count()
            stats['matched']['failure_count'] = len(matched_failures)
        else:
            for key in stats['matched']:
                stats['matched'][key] = sorted(stats['matched'][key])
            stats['matched']['failure_total'] = 0
            stats['matched']['failure_count'] = 0

        return JsonResponse(stats)

    def __convert_to_user_query__(self, request):
        params = json.loads(request.body.decode())
        filter = self.__parse_filter_from_params__(params)
        return JsonResponse({"userQuery": filter._to_user_query()})

    def post(self, request, *args, **kwargs):
        # Technically, this view cannot edit filters, but it deals with a lot of
        # input and it is only useful when editing issues, so it is safer to hide it
        if not request.user.has_perm('CIResults.change_issue'):
            raise PermissionDenied()

        action = kwargs.get('action')
        if action == "stats":
            return self.__stats__(request)
        if action == "convert-to-user-query":
            return self.__convert_to_user_query__(request)

        raise ValidationError("The action '{}' is unsupported".format(action))


class MassVettingView(PermissionRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        object_ids = set()
        for param in request.POST:
            try:
                object_ids.add(int(param))
            except ValueError:
                continue

        objects = self.model.objects.filter(pk__in=object_ids)
        for obj in objects:
            obj.vet()

        objects_str = [str(o) for o in objects]
        messages.success(self.request, "Successfully vetted the {} object(s): {}".format(len(objects),
                                                                                         ", ".join(objects_str)))

        return redirect('CIResults-index')


class MachineMassVettingView(MassVettingView):
    model = Machine
    permission_required = 'CIResults.vet_machine'


class TestMassVettingView(MassVettingView):
    model = Test
    permission_required = 'CIResults.vet_test'


class TextStatusMassVettingView(MassVettingView):
    model = TextStatus
    permission_required = 'CIResults.vet_textstatus'


class TestEditView(PermissionRequiredMixin, UpdateView):
    model = Test
    fields = ['public', 'vetted_on']
    template_name = 'CIResults/test_edit.html'
    permission_required = 'CIResults.change_test'

    def get_success_url(self):
        messages.success(self.request, "The test {} has been edited".format(self.object))
        return reverse("CIResults-tests")


class MachineEditView(PermissionRequiredMixin, UpdateView):
    model = Machine
    fields = ['description', 'public', 'vetted_on', 'aliases', 'color_hex', 'tags']
    template_name = 'CIResults/machine_edit.html'
    permission_required = 'CIResults.change_machine'

    def get_success_url(self):
        messages.success(self.request, "The machine {} has been edited".format(self.object))
        return reverse("CIResults-machines")


class TestMassRenameView(PermissionRequiredMixin, FormView):
    form_class = TestMassRenameForm
    template_name = 'CIResults/test_mass_rename.html'
    permission_required = 'CIResults.change_test'

    def get_success_url(self):
        return reverse("CIResults-tests")

    def form_valid(self, form):
        if not form.is_valid():
            return super().form_valid(form)

        if self.request.POST.get('action') == 'check':
            return render(self.request, self.template_name, {'form': form})
        else:
            form.do_renaming()
            messages.success(self.request,
                             "Renamed {} tests ('{}' -> '{}')".format(len(form.affected_tests),
                                                                      form.cleaned_data.get('substring_from'),
                                                                      form.cleaned_data.get('substring_to')))
            return super().form_valid(form)


class TestRenameView(PermissionRequiredMixin, UpdateView):
    model = Test
    fields = ['name']
    template_name = 'CIResults/test_rename.html'
    permission_required = 'CIResults.change_test'

    def form_valid(self, form):
        # We do not want to save any change, we just want to call the test's rename method
        self.object.rename(form.cleaned_data['name'])
        messages.success(self.request, "The test {} has been renamed to {}".format(self.object,
                                                                                   form.cleaned_data['name']))
        return HttpResponseRedirect(reverse("CIResults-tests"))


class filterView(DetailView):
    model = IssueFilter
    template_name = 'CIResults/filter.html'


class SimpleSearchableMixin:
    @property
    def query(self):
        return self.request.GET.get('q', '')

    # TODO: Add a nice UI for this
    def get_paginate_by(self, queryset):
        return self.request.GET.get('page_size', self.paginate_by)

    def get_context_data(self):
        context = super().get_context_data()
        context['search_query'] = self.query
        context['query_get_param'] = "q=" + self.query
        return context


class TestListView(ListView):
    model = Test
    paginator_class = SafePaginator
    paginate_by = 100

    def get_queryset(self):
        self.query = QueryCreator(self.request, self.model).multiple_request_params_to_query()
        tests = self.query.objects.order_by('testsuite__name', 'name').prefetch_related('testsuite',
                                                                                        'first_runconfig')
        return tests.exclude(first_runconfig=None)

    def get_context_data(self):
        context = super().get_context_data()
        context['query_get_param'] = self.request.GET.copy()
        return context


class MachineListView(ListView):
    model = Machine
    paginator_class = SafePaginator
    paginate_by = 100

    def get_queryset(self):
        self.query = QueryCreator(self.request, self.model).multiple_request_params_to_query()
        machines = self.query.objects.order_by('name').prefetch_related('aliases', 'tags')

        return machines

    def get_context_data(self):
        context = super().get_context_data()
        context['query_get_param'] = self.request.GET.copy()
        return context


class UserFiltrableMixin:
    # TODO: Add a nice UI for this
    def get_paginate_by(self, queryset):
        return self.request.GET.get('page_size', self.paginate_by)

    def get_context_data(self):
        context = super().get_context_data()
        context['query_get_param'] = self.request.GET.copy()
        context['query'] = self.query

        return context


class TestResultListView(UserFiltrableMixin, ListView):
    model = TestResult
    paginator_class = SafePaginator
    paginate_by = 100

    def get_userquery(self):
        return None

    def get_queryset(self):
        if self.get_userquery() is None:
            self.query = QueryCreator(self.request, self.model).request_to_query()
        else:
            self.query = self.model.from_user_filters(**self.get_userquery())

        if not self.query.is_empty:
            query = self.query.objects
            query = query.filter(ts_run__runconfig__temporary=False)  # Do not show results from temp runs
            query = query.prefetch_related('test',
                                           'test__testsuite',
                                           'ts_run',
                                           'ts_run__machine',
                                           'ts_run__runconfig',
                                           'ts_run__testsuite',
                                           'status',
                                           'status__testsuite',
                                           'known_failures__matched_ifa__filter',
                                           'known_failures__matched_ifa__issue__bugs__tracker')
            query = query.defer('stdout', 'stderr', 'dmesg')
            if self.query.orderby is None:
                query = query.order_by('-ts_run__runconfig__added_on')
            return query
        else:
            return self.model.objects.none().order_by('id')

    def get_context_data(self):
        context = super().get_context_data()
        context['results'] = context.pop('object_list')

        context["statuses_chart"] = metrics_testresult_statuses_stats(context['results']).stats()
        context["machines_chart"] = metrics_testresult_machines_stats(context['results']).stats()
        context["tests_chart"] = metrics_testresult_tests_stats(context['results']).stats()
        context["issues_chart"] = metrics_testresult_issues_stats(context['results']).stats()

        if self.get_userquery() is not None:
            context['filter_url'] = reverse('CIResults-results')

        return context

    def post(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class KnownFailureListView(UserFiltrableMixin, ListView):
    model = KnownFailure
    paginator_class = SafePaginator
    paginate_by = 100

    def get_userquery(self):
        return None

    def get_queryset(self):
        if self.get_userquery() is None:
            self.query = QueryCreator(self.request, self.model).request_to_query()
        else:
            self.query = self.model.from_user_filters(**self.get_userquery())

        if not self.query.is_empty:
            query = self.query.objects
            query = query.filter(result__ts_run__runconfig__temporary=False)  # Do not show results from temp runs
            query = query.prefetch_related('result__test',
                                           'result__test__testsuite',
                                           'result__ts_run',
                                           'result__ts_run__machine',
                                           'result__ts_run__runconfig',
                                           'result__ts_run__testsuite',
                                           'result__status',
                                           'result__status__testsuite',
                                           'matched_ifa__filter',
                                           'matched_ifa__issue__bugs__tracker')
            if self.query.orderby is None:
                query = query.order_by('-result__ts_run__runconfig__added_on')
            return query
        else:
            return self.model.objects.none().order_by('id')

    def get_context_data(self):
        context = super().get_context_data()
        context['failures'] = context.pop('object_list')

        context["statuses_chart"] = metrics_knownfailure_statuses_stats(context['failures']).stats()
        context["machines_chart"] = metrics_knownfailure_machines_stats(context['failures']).stats()
        context["tests_chart"] = metrics_knownfailure_tests_stats(context['failures']).stats()
        context["issues_chart"] = metrics_knownfailure_issues_stats(context['failures']).stats()

        if self.get_userquery() is not None:
            context['filter_url'] = reverse('CIResults-knownfailures')

        return context

    def post(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class IssueDetailView(KnownFailureListView):
    template_name = 'CIResults/issue_detail.html'

    @cached_property
    def issue(self):
        return get_object_or_404(Issue, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "issue_id={}".format(self.issue.id)}

    def get_context_data(self):
        context = super().get_context_data()
        context['issue'] = self.issue
        return context


class IFADetailView(KnownFailureListView):
    template_name = 'CIResults/ifa_detail.html'

    @cached_property
    def ifa(self):
        return get_object_or_404(IssueFilterAssociated, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "ifa_id={}".format(self.ifa.id)}

    def get_context_data(self):
        context = super().get_context_data()
        context['ifa'] = self.ifa
        return context


class TestSuiteDetailView(KnownFailureListView):
    template_name = 'CIResults/testsuite_detail.html'

    @cached_property
    def testsuite(self):
        return get_object_or_404(TestSuite, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "testsuite_name='{}'".format(self.testsuite.name)}

    def get_context_data(self):
        context = super().get_context_data()
        context['testsuite'] = self.testsuite
        return context


class TestDetailView(KnownFailureListView):
    template_name = 'CIResults/test_detail.html'

    @cached_property
    def test(self):
        return get_object_or_404(Test, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "test_name='{}'".format(self.test.name)}

    def get_context_data(self):
        context = super().get_context_data()
        context['test'] = self.test
        return context


class TextStatusDetailView(KnownFailureListView):
    template_name = 'CIResults/textstatus_detail.html'

    @cached_property
    def status(self):
        return get_object_or_404(TextStatus, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "status_name='{}'".format(self.status.name)}

    def get_context_data(self):
        context = super().get_context_data()
        context['status'] = self.status
        return context


class MachineDetailView(KnownFailureListView):
    template_name = 'CIResults/machine_detail.html'

    @cached_property
    def machine(self):
        return get_object_or_404(Machine, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "machine_name='{}'".format(self.machine.name)}

    def get_context_data(self):
        context = super().get_context_data()
        context['machine'] = self.machine
        return context


class TestResultDetailView(TestResultListView):
    template_name = 'CIResults/testresult_detail.html'

    @cached_property
    def testresult(self):
        return get_object_or_404(TestResult, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "test_name='{}' AND machine_name='{}'".format(self.testresult.test.name,
                                                                       self.testresult.ts_run.machine.name)}

    def get_context_data(self):
        context = super().get_context_data()
        context['testresult'] = self.testresult
        return context


class RunConfigDetailView(KnownFailureListView):
    template_name = 'CIResults/runconfig_detail.html'

    @cached_property
    def runconfig(self):
        return get_object_or_404(RunConfig, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "runconfig_name='{}'".format(self.runconfig.name)}

    def get_context_data(self):
        context = super().get_context_data()
        context['runconfig'] = self.runconfig
        return context


class RunConfigTagDetailView(KnownFailureListView):
    template_name = 'CIResults/runconfig_tag_detail.html'

    @cached_property
    def tag(self):
        return get_object_or_404(RunConfigTag, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "runconfig_tag='{}'".format(self.tag.name)}

    def get_context_data(self):
        context = super().get_context_data()
        context['tag'] = self.tag
        return context


class BuildDetailView(KnownFailureListView):
    template_name = 'CIResults/build_detail.html'

    @cached_property
    def build(self):
        return get_object_or_404(Build, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "build_name='{}'".format(self.build.name)}

    def get_context_data(self):
        context = super().get_context_data()
        context['build'] = self.build
        return context


class ComponentDetailView(KnownFailureListView):
    template_name = 'CIResults/component_detail.html'

    @cached_property
    def component(self):
        return get_object_or_404(Component, pk=self.kwargs['pk'])

    def get_userquery(self):
        return {'query': "component_name='{}'".format(self.component.name)}

    def get_context_data(self):
        context = super().get_context_data()
        context['component'] = self.component
        return context


class ResultsCompareView(TemplateView):
    template_name = "CIResults/compare_results.html"

    def name_to_runconfig(self, name):
        if name is None:
            return None, False

        run = RunConfig.objects.filter(name=name).first()
        return run, run is None

    def urlify(self, markdown):
        regex = re.compile(r'(https?:\/\/[^\s,;]+)')
        return regex.sub(r'<\1>', markdown)

    def get_context_data(self):
        run_from, from_error = self.name_to_runconfig(self.request.GET.get('from'))
        run_to, to_error = self.name_to_runconfig(self.request.GET.get('to'))
        no_compress = self.request.GET.get('nocompress') is not None
        query = QueryCreator(self.request, TestResult).request_to_query()

        context = super().get_context_data()
        context['runconfigs'] = RunConfig.objects.filter(temporary=False).order_by('-added_on')[0:50]
        context['from_error'] = from_error
        context['to_error'] = to_error
        context['no_compress'] = no_compress
        context['filters_model'] = TestResult
        context['query'] = query

        if run_from and run_to:
            diff = run_from.compare(run_to, no_compress=no_compress, query=query)
            context['diff'] = diff
            context['html_result'] = mark_safe(markdown.markdown(self.urlify(diff.text),
                                                                 extensions=['nl2br']))

        return context
