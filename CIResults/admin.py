from django.contrib import admin
from django.forms import ModelForm, ModelMultipleChoiceField, PasswordInput
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.admin import UserAdmin as BaseUserModel
from .models import BugTracker, Bug, Component, Build, Test, Machine, RunConfigTag, RunConfig, TestSuite, TestsuiteRun
from .models import TextStatus, TestResult, IssueFilter, IssueFilterAssociated, Issue, KnownFailure, UnknownFailure
from .models import RunFilterStatistic, MachineTag, BugTrackerSLA, BugTrackerAccount, Person, BugComment
from .models import ReplicationScript, User


@admin.register(ReplicationScript)
class ReplicationScriptModel(admin.ModelAdmin):
    list_display = ['name']


class BugTrackerForm(ModelForm):
    class Meta:
        model = BugTracker
        exclude = []
        widgets = {
            "password": PasswordInput(),
        }


@admin.register(BugTracker)
class BugTrackerModel(admin.ModelAdmin):
    list_display = ('name', 'public')
    search_fields = ['name']
    readonly_fields = ['polled']
    form = BugTrackerForm


@admin.register(Bug)
class BugModel(admin.ModelAdmin):
    readonly_fields = ['title', 'created', 'updated', 'polled', 'creator', 'product',
                       'assignee', 'component', 'features', 'platforms', 'status', 'priority',
                       'severity', 'tags', 'parent']
    search_fields = ['bug_id']


@admin.register(BugTrackerSLA)
class BugTrackerSLAModel(admin.ModelAdmin):
    list_display = ('tracker', 'priority', 'SLA')
    search_fields = ('tracker__name', 'priority', 'SLA')


@admin.register(Person)
class PersonModel(admin.ModelAdmin):
    list_display = ('full_name', 'email')
    search_fields = ('full_name', 'email')


@admin.register(BugTrackerAccount)
class BugTrackerAccountModel(admin.ModelAdmin):
    list_display = ('tracker', 'person', 'user_id')
    search_fields = ('tracker__name', 'person__email', 'person__full_name', 'user_id')


@admin.register(BugComment)
class BugCommentModel(admin.ModelAdmin):
    list_display = ('bug', 'account', 'url', 'created_on')
    search_fields = ('bug__tracker__short_name', 'bug__bug_id', 'bug__title',
                     'account__person__full_name', 'account__person__email',
                     'url', 'created_on')


@admin.register(Component)
class ComponentModel(admin.ModelAdmin):
    list_display = ('name', 'public')
    search_fields = ['name']


@admin.register(Build)
class BuildModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    list_display = ['name']
    search_fields = ['name', 'version', 'branch', 'repo']
    autocomplete_fields = ['parents']


@admin.register(Test)
class TestModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    list_display = ('name', 'testsuite', 'public', 'vetted_on')
    search_fields = ['name']


@admin.register(MachineTag)
class MachineTagModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    list_display = ('name', 'public')
    search_fields = ['name', 'description']


@admin.register(Machine)
class MachineModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    list_display = ('name', 'public', 'vetted_on')
    search_fields = ['name', 'description']


@admin.register(RunConfigTag)
class RunConfigTagModel(admin.ModelAdmin):
    list_display = ('name', 'public')
    search_fields = ['name', 'description']


@admin.register(RunConfig)
class RunModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    list_display = ['name']
    search_fields = ['name', 'tags__name']
    filter_horizontal = ['tags', 'builds']
    list_filter = ('added_on', 'temporary')


@admin.register(TestSuite)
class TestSuiteModel(admin.ModelAdmin):
    list_display = ('__str__', 'public', 'vetted_on')
    search_fields = ['name', 'description']
    filter_horizontal = ['acceptable_statuses']


@admin.register(TestsuiteRun)
class TestsuiteRunModel(admin.ModelAdmin):
    list_display = ('runconfig', 'machine', 'run_id')
    search_fields = ['runconfig__name', 'machine__name', 'run_id']


@admin.register(TextStatus)
class TextStatusModel(admin.ModelAdmin):
    list_display = ('__str__', 'testsuite', 'vetted_on', 'severity')


@admin.register(TestResult)
class TestResultdModel(admin.ModelAdmin):
    readonly_fields = ['test', 'ts_run', 'status', 'command', 'stdout', 'stderr', 'dmesg']
    list_select_related = ['test', 'ts_run__runconfig', 'ts_run__machine']
    show_full_result_count = False


class IssueFilterForm(ModelForm):
    class Meta:
        model = IssueFilter
        exclude = []
    tests = ModelMultipleChoiceField(queryset=Test.objects.prefetch_related('testsuite').all(),
                                     help_text="The result's machine should be one of these tests "
                                               "(leave empty to ignore tests)",
                                     widget=FilteredSelectMultiple('tests', False))
    statuses = ModelMultipleChoiceField(queryset=TextStatus.objects.prefetch_related('testsuite').all(),
                                        help_text="The result's status should be one of these "
                                                  "(leave empty to ignore results)",
                                        widget=FilteredSelectMultiple('statuses', False))


@admin.register(IssueFilter)
class IssueFilterModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    filter_horizontal = ['machines', 'tests', 'statuses', 'tags']
    form = IssueFilterForm

    # Prevent deletion/modification, as we want data from
    def has_delete_permission(self, request, obj=None):
        return False  # pragma: no cover


@admin.register(IssueFilterAssociated)
class IssueFilterAssociatedModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    raw_id_fields = ['filter', 'issue']
    list_select_related = ['filter', 'issue']
    show_full_result_count = False


class FilterInline(admin.TabularInline):
    readonly_fields = ['added_on']
    model = IssueFilterAssociated
    extra = 1


@admin.register(Issue)
class IssueModel(admin.ModelAdmin):
    readonly_fields = ['added_on']
    inlines = [FilterInline]


@admin.register(KnownFailure)
class KnownFailureModel(admin.ModelAdmin):
    raw_id_fields = ['result', 'matched_ifa']
    list_select_related = ['result__test', 'result__ts_run__runconfig', 'result__ts_run__machine', "result__status"]
    show_full_result_count = False


@admin.register(UnknownFailure)
class UnknownFailureModel(admin.ModelAdmin):
    raw_id_fields = ['result']
    list_select_related = ['result__test', 'result__ts_run__runconfig', 'result__ts_run__machine', 'result__status']
    show_full_result_count = False


@admin.register(RunFilterStatistic)
class RunFilterStatisticModel(admin.ModelAdmin):
    list_display = ('__str__', 'covered_count', 'matched_count')
    raw_id_fields = ['filter', 'runconfig']
    list_select_related = ['runconfig', 'filter']


@admin.register(User)
class UserModel(BaseUserModel):
    # The default admin model for User needs to be unregistered, before registering new functionalities
    admin.site.unregister(User)

    class UserTypeFilter(admin.SimpleListFilter):
        title = 'user type'
        parameter_name = "user_type"

        def lookups(self, request, model_admin):  # pragma: nocover
            return [
                ('is_normal_user', 'Normal user'),
                ('is_staff', 'Staff member'),
            ]

        def queryset(self, request, queryset):  # pragma: nocover
            if self.value() == 'is_normal_user':
                return queryset.filter(is_staff__exact=False, is_superuser__exact=False)
            if self.value() == 'is_staff':
                return queryset.filter(is_staff__exact=True)

    list_filter = [UserTypeFilter, 'last_login']
