import json
import datetime

from unittest.mock import patch, MagicMock
from rest_framework.test import APIRequestFactory, APITestCase
from django.conf import settings
from django.db import transaction
from django.test import TestCase
from django.http.response import Http404
from django.urls import reverse
from model_bakery import baker

from CIResults.models import (
    Bug,
    BugTracker,
    Build,
    Component,
    Issue,
    IssueFilter,
    Machine,
    MachineTag,
    RunConfig,
    RunConfigTag,
    Test,
    TestResult,
    TestSuite,
    TextStatus,
    UnknownFailure,
)
from CIResults.rest_views import CustomPagination, IssueFilterViewSet, RunConfigViewSet, get_obj_by_id_or_name
from CIResults.rest_views import BuildViewSet
from CIResults.tests.test_views import create_user_and_log_in

from shortener.models import Shortener

# HACK: Massively speed up the login primitive. We don't care about security in tests
settings.PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )


class UtilsTests(TestCase):
    def test_get_obj_by_id_or_name__id(self):
        rc = RunConfig.objects.create(name='valid', temporary=True)
        self.assertEqual(get_obj_by_id_or_name(RunConfig, 1), rc)

    def test_get_obj_by_id_or_name__name(self):
        rc = RunConfig.objects.create(name='valid', temporary=True)
        self.assertEqual(get_obj_by_id_or_name(RunConfig, "valid"), rc)

    def test_get_obj_by_id_or_name__not_exist(self):
        self.assertRaises(RunConfig.DoesNotExist, get_obj_by_id_or_name, RunConfig, 1)


class CustomPaginationTests(TestCase):
    def get_page_size(self, query_params, page_size_query_param='page_size', max_page_size=None):
        request = MagicMock(query_params=query_params)

        pagination = CustomPagination()
        pagination.page_size_query_param = page_size_query_param
        pagination.max_page_size = max_page_size

        return pagination.get_page_size(request)

    def test_default_page_size(self):
        self.assertEqual(self.get_page_size({}), CustomPagination.page_size)

    def test_default_page_size_without_page_size_field(self):
        self.assertEqual(self.get_page_size({}, page_size_query_param=None), CustomPagination.page_size)

    def test_invalid_page_size(self):
        self.assertEqual(self.get_page_size({'page_size': 'toto'}), CustomPagination.page_size)

    def test_negative_page_size(self):
        self.assertEqual(self.get_page_size({CustomPagination.page_size_query_param: -0}), None)

    def test_page_size_too_big(self):
        self.assertEqual(self.get_page_size({'page_size': '1000'}, max_page_size=10), 10)

    def test_page_size_big_but_no_limits(self):
        self.assertEqual(self.get_page_size({'page_size': '1000'}, max_page_size=None), 1000)

    def test_acceptable_page_size(self):
        self.assertEqual(self.get_page_size({'page_size2': '42'}, page_size_query_param='page_size2'), 42)


class IssueFilterTests(APITestCase):
    maxDiff = None

    def setUp(self):
        self.view = IssueFilterViewSet()
        self.user = None

    def __post__(self, body_dict, logged_in=True, permissions=['add_issuefilter']):
        if logged_in:
            self.user = create_user_and_log_in(self.client, permissions=permissions)

        return self.client.post(reverse('api:issuefilter-list'), body_dict, format='json')

    def test__get_or_None__empty_field(self):
        errors = []
        self.assertIsNone(self.view.__get_or_None__(Machine, 'field', {'field': ''}, errors))
        self.assertEqual(errors, [])

    def test__get_or_None__invalid_id(self):
        errors = []
        self.assertIsNone(self.view.__get_or_None__(Machine, 'field', {'field': '12'}, errors))
        self.assertEqual(errors, ["The object referenced by 'field' does not exist"])

    def test_get_filter_by_description(self):
        IssueFilter.objects.create(description="filter one")
        IssueFilter.objects.create(description="filter two")
        response = self.client.get(reverse('api:issuefilter-list'), {'description': 'one'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["description"], "filter one")

    def test_create_empty(self):
        response = self.__post__({})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, ["The field 'description' cannot be empty"])

    def test_invalid_regexps(self):
        response = self.__post__({"description": "Minimal IssueFilter",
                                  "stdout_regex": '(', "stderr_regex": '(',
                                  "dmesg_regex": '('})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, ["The field 'stdout_regex' does not contain a valid regular expression",
                                         "The field 'stderr_regex' does not contain a valid regular expression",
                                         "The field 'dmesg_regex' does not contain a valid regular expression"])

    def test_create_minimal__unauthenticated(self):
        response = self.__post__({"description": "Minimal IssueFilter"}, logged_in=False)
        self.assertEqual(response.status_code, 401)

    # FIXME: This is not currently working
    # def test_create_minimal__invalid_permissions(self):
        # response = self.__post__({"description": "Minimal IssueFilter"}, logged_in=True, permissions=[])
        # self.assertEqual(response.status_code, 403)

    def test_create_minimal(self):
        response = self.__post__({"description": "Minimal IssueFilter"})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data,
                         {'id': response.data['id'], 'description': 'Minimal IssueFilter',
                          'tags': [], 'machine_tags': [], 'machines': [], 'tests': [],
                          'statuses': [], 'stdout_regex': '',
                          'stderr_regex': '', 'dmesg_regex': '',
                          'user_query': '', '__str__': 'Minimal IssueFilter'})

    def test_create_invalid(self):
        response = self.__post__({
                "description": "Invalid tags, machines, tests, and statuses",
                'tags': [1], 'machines': [2], 'tests': [3], 'statuses': [4]
        })
        self.assertEqual(response.status_code, 400, response.data)
        self.assertEqual(response.data, ['At least one tag does not exist',
                                         'At least one machine does not exist',
                                         'At least one test does not exist',
                                         'At least one status does not exist'])

    def test_create_complete(self):
        # Create some objects before referencing them
        ts = TestSuite.objects.create(name="ts", description="", url="", public=True)
        for i in range(1, 6):
            RunConfigTag.objects.create(id=i, name="tag{}".format(i), description="", url="", public=True)
            Machine.objects.create(id=i, name="machine{}".format(i), public=True)
            Test.objects.create(id=i, name="test{}".format(i), testsuite=ts, public=True)
            TextStatus.objects.create(id=i, testsuite=ts, name="status{}".format(i))
            MachineTag.objects.create(id=i, name="TAG{}".format(i), public=True)

        # Make the request and check that we get the expected output
        with transaction.atomic():
            response = self.__post__({
                "description": "Minimal IssueFilter",
                'tags': [1, 2], 'machine_tags': [1, 5], 'machines': [2, 3], 'tests': [3, 4], 'statuses': [4, 5],
                'stdout_regex': 'stdout', 'stderr_regex': 'stderr', 'dmesg_regex': 'dmesg'
                })

        self.assertEqual(response.status_code, 201, response.data)
        self.assertEqual(dict(response.data),
                         {'id': response.data['id'], 'description': 'Minimal IssueFilter',
                          'tags': [
                              {'id': 1, 'description': '', 'url': '', 'public': True, '__str__': 'tag1'},
                              {'id': 2, 'description': '', 'url': '', 'public': True, '__str__': 'tag2'}
                          ],
                          'machine_tags': [
                              {'id': 1, 'name': 'TAG1', 'public': True},
                              {'id': 5, 'name': 'TAG5', 'public': True},
                          ],
                          'machines': [
                              {'id': 2, 'vetted_on': None, 'public': True, '__str__': 'machine2'},
                              {'id': 3, 'vetted_on': None, 'public': True, '__str__': 'machine3'},
                              ],
                          'tests': [
                              {'id': 3, 'testsuite': {'id': ts.id, '__str__': 'ts'}, 'public': True,
                               'vetted_on': None, 'first_runconfig': None, 'name': 'test3', '__str__': 'ts: test3'},
                              {'id': 4, 'testsuite': {'id': ts.id, '__str__': 'ts'}, 'public': True,
                               'vetted_on': None, 'first_runconfig': None, 'name': 'test4', '__str__': 'ts: test4'},
                          ],
                          'statuses': [
                              {'id': 4, 'testsuite': {'id': ts.id, '__str__': 'ts'}, 'name': 'status4',
                               '__str__': 'ts: status4'},
                              {'id': 5, 'testsuite': {'id': ts.id, '__str__': 'ts'}, 'name': 'status5',
                               '__str__': 'ts: status5'},
                          ],
                          'stdout_regex': 'stdout', 'stderr_regex': 'stderr',
                          'dmesg_regex': 'dmesg', '__str__': 'Minimal IssueFilter',
                          'user_query': ''})

    def test_edit_invalid(self):
        response = self.__post__({"description": "new filter",
                                  "edit_filter": "a", "edit_issue": "b"})
        self.assertEqual(response.status_code, 400, response.data)
        self.assertEqual(response.data, ["The field 'edit_filter' needs to be an integer",
                                         "The field 'edit_issue' needs to be an integer"])

    @patch('CIResults.models.IssueFilter.replace')
    @patch('CIResults.models.Issue.replace_filter')
    def test_edit_all_issues(self, mock_replace_filter, mock_filter_replace):
        filter = IssueFilter.objects.create(description="old filter")

        with transaction.atomic():
            response = self.__post__({"description": "new filter", "edit_filter": filter.id})
        self.assertEqual(response.status_code, 201, response.data)
        self.assertEqual(response.data,
                         {'id': response.data['id'], 'description': 'new filter',
                          'tags': [], 'machine_tags': [], 'machines': [], 'tests': [],
                          'statuses': [], 'stdout_regex': '',
                          'stderr_regex': '', 'dmesg_regex': '',
                          'user_query': '', '__str__': 'new filter'})

        new_filter = IssueFilter.objects.get(pk=response.data['id'])
        mock_replace_filter.assert_not_called()
        mock_filter_replace.assert_called_once_with(new_filter, self.user)
        self.assertEqual(mock_filter_replace.call_args_list[0][0][0].id, response.data['id'])

    @patch('CIResults.models.IssueFilter.replace')
    @patch('CIResults.models.Issue.replace_filter')
    def test_edit_one_issue(self, mock_replace_filter, mock_filter_replace):
        filter = IssueFilter.objects.create(description="desc")
        issue = Issue.objects.create(description="issue")

        with transaction.atomic():
            response = self.__post__({"description": "new filter",
                                      "edit_filter": "{}".format(filter.id),
                                      "edit_issue": issue.id})

        self.assertEqual(response.status_code, 201, response.data)
        self.assertEqual(response.data,
                         {'id': response.data['id'], 'description': 'new filter',
                          'tags': [], 'machine_tags': [], 'machines': [], 'tests': [],
                          'statuses': [], 'stdout_regex': '',
                          'stderr_regex': '', 'dmesg_regex': '',
                          'user_query': '', '__str__': 'new filter'})

        new_filter = IssueFilter.objects.get(pk=response.data['id'])
        mock_filter_replace.assert_not_called()
        mock_replace_filter.assert_called_once_with(filter, new_filter, self.user)
        self.assertEqual(mock_replace_filter.call_args_list[0][0][0].id, filter.id)
        self.assertEqual(mock_replace_filter.call_args_list[0][0][1].id, response.data['id'])


class RunConfigTests(TestCase):
    def setUp(self):
        self.view = RunConfigViewSet()
        self.arf = APIRequestFactory()

    @patch('CIResults.models.RunConfig.objects.get')
    def test_get_runcfg__by_id(self, runcfg_get_mocked):
        self.assertEqual(self.view._get_runcfg('1'), runcfg_get_mocked.return_value)
        runcfg_get_mocked.assert_called_once_with(pk=1)

    @patch('CIResults.models.RunConfig.objects.get')
    def test_get_runcfg__by_invalid_name(self, runcfg_get_mocked):
        self.assertRaises(Http404, self.view._get_runcfg, 'missing')
        runcfg_get_mocked.assert_not_called()

    @patch('CIResults.models.RunConfig.objects.get')
    def test_get_runcfg__by_valid_name(self, runcfg_get_mocked):
        rc = RunConfig.objects.create(name='valid', temporary=True)
        self.assertEqual(self.view._get_runcfg('valid').pk, rc.id)
        runcfg_get_mocked.assert_not_called()

    def test_retrieve__by_id(self):
        rc = RunConfig.objects.create(name='valid', temporary=True)
        data = self.view.retrieve(self.arf.get("/", {}, format="json"), pk="valid").data
        self.assertEqual(data["id"], rc.id)
        self.assertEqual(data["name"], "valid")
        self.assertEqual(data["tags"], [])
        self.assertIsNone(data["url"])
        self.assertIsNotNone(data["added_on"])

    def test_known_failures(self):
        # TODO: Revisit the test whenever we start generating fixtures
        RunConfig.objects.create(name='valid', temporary=True)
        response = self.view.known_failures(self.arf.get('/', {}, format='json'),
                                            pk='valid')
        self.assertEqual(response.data, [])

    @patch('CIResults.serializers.RunConfigDiffSerializer.data')
    @patch('CIResults.models.RunConfig.compare')
    def test_compare(self, compare_mocked, serializer_mocked):
        RunConfig.objects.create(name='runcfg1', temporary=True)
        runcfg2 = RunConfig.objects.create(name='runcfg2', temporary=True)
        response = self.view.compare(self.arf.get('/', {'to': 'runcfg2'}, format='json'),
                                     pk='runcfg1')
        compare_mocked.assert_called_once_with(runcfg2, no_compress=False)
        self.assertEqual(response.data, serializer_mocked)

    @patch('CIResults.models.RunConfig.compare')
    def test_compare__no_compress(self, compare_mocked):
        RunConfig.objects.create(name='runcfg1', temporary=True)
        runcfg2 = RunConfig.objects.create(name='runcfg2', temporary=True)
        self.view.compare(self.arf.get('/', {'to': 'runcfg2', 'no_compress': '1'}, format='json'),
                          pk='runcfg1')
        compare_mocked.assert_called_once_with(runcfg2, no_compress=True)

    def test_compare__only_summary(self):
        RunConfig.objects.create(name='runcfg1', temporary=True)
        RunConfig.objects.create(name='runcfg2', temporary=True)
        response = self.view.compare(self.arf.get('/', {'to': 'runcfg2', 'summary': ''}, format='json'),
                                     pk='runcfg1')
        self.assertIn('CI Bug Log', response.content.decode())

    def test_create__no_permissions(self):
        response = self.client.post(reverse('api:runconfig-list'),
                                    data={"name": "runcfg", "tags": ["tag1"], "temporary": False},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 401)

    def test_create(self):
        RunConfigTag.objects.create(id=1, name="tag1", public=True)
        create_user_and_log_in(self.client, permissions=['add_runconfig'])
        response = self.client.post(reverse('api:runconfig-list'),
                                    data={"name": "runcfg", "tags": ["tag1"], "builds": [], "temporary": False},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["name"], "runcfg")
        self.assertEqual(response.data["tags"], ["tag1"])

    def test_create__invalid_data(self):
        create_user_and_log_in(self.client, permissions=['add_runconfig'])
        response = self.client.post(reverse('api:runconfig-list'), data={"name": "runcfg"},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 400)

    def test_create__invalid_data_missing_tag(self):
        create_user_and_log_in(self.client, permissions=['add_runconfig'])
        response = self.client.post(reverse('api:runconfig-list'),
                                    data={"name": "runcfg", "tags": ["tag1"], "temporary": False},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 400)

    def test_import_testsuite_run(self):
        RunConfig.objects.create(name='runcfg1', temporary=False)
        component = TestSuite.objects.create(id=1, name='testsuite', public=True)
        Build.objects.create(name='testsuite-1', component=component)
        create_user_and_log_in(self.client, admin=True)
        response = self.client.post("/api/runconfig/runcfg1/testsuiterun/", content_type="application/json", data={
            "test_suite": "testsuite-1",
            "test_results": {
                "machine-1": {
                    "001": {
                        "test1": {
                            "status": "pass",
                            "stdout": "output"
                        }
                    },
                    "002": {
                        "test2": {
                            "status": "pass",
                            "stdout": "output"
                        }
                    }
                }
            }
        })
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(Machine.objects.get(name="machine-1"))
        self.assertIsNotNone(Test.objects.get(name="test1"))
        self.assertIsNotNone(TextStatus.objects.get(name="pass"))
        self.assertEqual(TestResult.objects.count(), 2)
        for test_name in ["test1", "test2"]:
            result = TestResult.objects.filter(test__name=test_name).first()
            self.assertEqual(result.status.name, "pass")
            self.assertEqual(result.stdout, "output")

    def test_import_testsuite_run__no_permissions(self):
        response = self.client.post("/api/runconfig/runcfg1/testsuiterun/", content_type="application/json", data={})
        self.assertEqual(response.status_code, 401)

    def test_import_testsuite_run__invalid_data(self):
        RunConfig.objects.create(name='runcfg1', temporary=False)
        component = TestSuite.objects.create(id=1, name='testsuite', public=True)
        Build.objects.create(name='testsuite-1', component=component)
        create_user_and_log_in(self.client, admin=True)
        response = self.client.post("/api/runconfig/runcfg1/testsuiterun/", content_type="application/json", data={
            "test_suite": "testsuite-1",
            "test_results": "invalid_data"
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()["message"]["test_results"],
                         ['Expected a dictionary of items but got type "str".'])

    def test_import_testsuite_run__invalid_data_no_testsuite(self):
        RunConfig.objects.create(name='runcfg1', temporary=False)
        create_user_and_log_in(self.client, admin=True)
        response = self.client.post("/api/runconfig/runcfg1/testsuiterun/", content_type="application/json", data={
            "test_suite": "testsuite-1",
            "test_results": {
                "machine-1": {
                    "001": {
                        "test1": {
                            "status": "pass",
                            "stdout": "output"
                        }
                    }
                }
            }
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()["message"], 'Testsuite build testsuite-1 does not exist')


class BuildViewSetTests(APITestCase):
    def setUp(self) -> None:
        self.component = Component.objects.create(name="component", public=False)
        self.view = BuildViewSet()
        self.arf = APIRequestFactory()

    def test_retrieve(self):
        rc = Build.objects.create(name='valid', component=self.component)
        data = self.view.retrieve(self.arf.get("/", {}, format="json"), pk="valid").data
        self.assertEqual(data["id"], rc.id)
        self.assertEqual(data["name"], "valid")
        self.assertIsNotNone(data["added_on"])

    def test_create_build_without_permissions(self):
        response = self.client.post(reverse('api:build-list'),
                                    data={"name": "test-build", "component": "component", "version": "1"},
                                    format="json")
        self.assertEqual(response.status_code, 401)

    def test_create_build(self):
        create_user_and_log_in(self.client, permissions=['add_build'])
        response = self.client.post(reverse('api:build-list'),
                                    data={"name": "build1", "component": "component", "version": "1", "parents": []},
                                    format="json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["name"], "build1")

    def test_create_build__invalid_data_schema(self):
        create_user_and_log_in(self.client, permissions=['add_build'])
        response = self.client.post(reverse('api:build-list'),
                                    data={"name": "test-build", "component": "component"},
                                    format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)["version"], ["This field is required."])

    def test_create_build__invalid_data(self):
        create_user_and_log_in(self.client, permissions=['add_build'])
        response = self.client.post(reverse('api:build-list'),
                                    data={"name": "test-build", "component": "no-component", "version": "1"},
                                    format="json")
        self.assertEqual(response.status_code, 400)


class BugViewSetTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()

    @classmethod
    def setUpTestData(cls):
        cls.bt1 = BugTracker.objects.create(name='Test Suite 1', short_name='ts1', public=True)
        cls.bt2 = BugTracker.objects.create(name='Test Suite 2', short_name='ts2', public=True)

        cls.bugs = dict()
        for bt in (cls.bt1, cls.bt2):
            cls.bugs[bt] = []
            for i in range(2):
                cls.bugs[bt].append(Bug.objects.create(tracker=bt, bug_id='b_'+str(i)))

    def test_retrieving_by_tracker_id(self):
        tracker = self.bt2
        bug = self.bugs[self.bt2][1]
        response = self.client.get(reverse('api-bugtracker-get-bug',
                                           kwargs={'tracker': tracker.id, 'bug_id': bug.bug_id}), format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['bug_id'], bug.bug_id)
        self.assertEqual(response.data['tracker']['short_name'], tracker.short_name)

    def test_retrieving_by_tracker_name(self):
        tracker = self.bt1
        bug = self.bugs[self.bt2][0]
        response = self.client.get(reverse('api-bugtracker-get-bug',
                                           kwargs={'tracker': tracker.name, 'bug_id': bug.bug_id}), format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['bug_id'], bug.bug_id)
        self.assertEqual(response.data['tracker']['short_name'], tracker.short_name)

    def test_retrieving_by_tracker_short_name(self):
        tracker = self.bt2
        bug = self.bugs[self.bt2][0]
        response = self.client.get(reverse('api-bugtracker-get-bug',
                                           kwargs={'tracker': tracker.short_name, 'bug_id': bug.bug_id}), format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['bug_id'], bug.bug_id)
        self.assertEqual(response.data['tracker']['short_name'], tracker.short_name)


class ShortenerViewSetTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()

    @classmethod
    def setUpTestData(cls):
        cls.short1 = Shortener.get_or_create("Some sort of long query!")

    def test_create_invalid_request(self):
        with self.assertRaisesMessage(ValueError, "Only JSON POST requests are supported"):
            self.client.post(reverse('api:shortener-list'), data={})

    def test_create_empty(self):
        with self.assertRaisesMessage(ValueError,
                                      "Missing the field 'full' which should contain the full text to be shortened"):
            self.client.post(reverse('api:shortener-list'), data={}, content_type="application/json")

    def test_create_single(self):
        full = 'Some sort of long query, but different!'

        response = self.client.post(reverse('api:shortener-list'), data={'full': full}, content_type="application/json")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertNotEqual(data['id'], self.short1.id)
        self.assertEqual(data['full'], full)

    def test_create_multiple(self):
        fulls = ["query1", "query2"]

        response = self.client.post(reverse('api:shortener-list'), data={'full': fulls},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(data[0]['full'], fulls[0])
        self.assertEqual(data[1]['full'], fulls[1])

    def test_retrieving_existing(self):
        response = self.client.post(reverse('api:shortener-list'), data={'full': self.short1.full},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 200)

        data = response.json()
        self.assertEqual(data['id'], self.short1.id)


class MachineViewSetTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()

    def test_list_machines(self):
        Machine.objects.create(name="machine_1", public=True)
        response = self.client.get(reverse('api:machine-list'), content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content)["results"],
                         [{'id': 1,
                           'name': 'machine_1',
                           'description': None,
                           'public': True,
                           'vetted_on': None,
                           'aliases': None,
                           'tags': []}])

    def test_create_machine_without_permission(self):
        response = self.client.post(reverse('api:machine-list'), data={'name': 'machine_1', 'tags': ['tag1']},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 401)

    def test_create_machine(self):
        self.user = create_user_and_log_in(self.client, permissions=['add_machine'])
        response = self.client.post(reverse('api:machine-list'), data={'name': 'machine_1', 'tags': ['tag1']},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(json.loads(response.content), {
            "data": {
                "id": 1,
                "name": "machine_1",
                "description": None,
                "public": False,
                "vetted_on": None,
                "aliases": None,
                "tags": ["tag1"]

            },
            "status": "success"
        })

    def test_create_machine_invalid_data(self):
        self.user = create_user_and_log_in(self.client, permissions=['add_machine'])
        response = self.client.post(reverse('api:machine-list'), data={'name': True},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content), {
            "message": {"name": ["Not a valid string."]},
            "status": "error"
        })

    def test_create_machine_import_error(self):
        self.user = create_user_and_log_in(self.client, permissions=['add_machine'])
        response = self.client.post(reverse('api:machine-list'), data={'name': 'machine_1', 'alias': 'machine_2'},
                                    content_type="application/json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content), {
            "message": "The machine this machine is supposed to alias does not exist. "
                       "Create it first...",
            "status": "error"
        })

    def test_vet_machine_without_permission(self):
        machine = baker.make(Machine)
        self.user = create_user_and_log_in(self.client, admin=False)
        response = self.client.post(reverse('api:machine-vet', kwargs={"pk": machine.pk}))
        self.assertEqual(response.status_code, 403)

    @patch("django.utils.timezone.now")
    def test_vet_machine(self, now_mocked):
        date = datetime.datetime(2021, 1, 1, tzinfo=datetime.timezone.utc)
        now_mocked.return_value = date
        machine = baker.make(Machine)
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.post(reverse("api:machine-vet", kwargs={"pk": machine.pk}))
        machine.refresh_from_db()
        self.assertEqual(machine.vetted_on, date)
        self.assertEqual(response.status_code, 200)

    def test_vet_already_vetted(self):
        date = datetime.datetime(2021, 1, 1, tzinfo=datetime.timezone.utc)
        machine = baker.make(Machine, vetted_on=date)
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.post(reverse('api:machine-vet', kwargs={"pk": machine.pk}))
        machine.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(machine.vetted_on, date)

    def test_suppress_machine_without_permission(self):
        machine = baker.make(Machine)
        self.user = create_user_and_log_in(self.client, admin=False)
        response = self.client.post(reverse('api:machine-suppress', kwargs={"pk": machine.pk}))
        self.assertEqual(response.status_code, 403)

    def test_suppress_machine(self):
        machine = baker.make(Machine, vetted_on="2021-01-01")
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.post(reverse('api:machine-suppress', kwargs={"pk": machine.pk}))
        self.assertEqual(response.status_code, 200)


class IssueViewSetTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()

    def test_archive_issue_without_permission(self):
        response = self.client.get(reverse('api:issue-archive', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {"message": "User AnonymousUser doesn't have sufficient permissions"})

    def test_archive_issue(self):
        Issue.objects.create()
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.get(reverse('api:issue-archive', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 200)

    def test_archive_archived_issue(self):
        self.user = create_user_and_log_in(self.client, admin=True)
        Issue.objects.create().archive(self.user)
        response = self.client.get(reverse('api:issue-archive', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"message": "The issue is already archived"})

    def test_restore_issue_without_permission(self):
        response = self.client.get(reverse('api:issue-restore', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {"message": "User AnonymousUser doesn't have sufficient permissions"})

    def test_restore_issue(self):
        self.user = create_user_and_log_in(self.client, admin=True)
        Issue.objects.create().archive(self.user)
        response = self.client.get(reverse('api:issue-restore', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 200)

    def test_restore_not_archived_issue(self):
        Issue.objects.create()
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.get(reverse('api:issue-restore', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"message": "The issue is not currently archived"})

    def test_update_to_expected(self):
        Issue.objects.create(expected=False)
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.patch(reverse('api:issue-detail', kwargs={"pk": 1}), data={"id": 1, "expected": True},
                                     content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["expected"], True)
        self.assertEqual(Issue.objects.get(pk=1).expected, True)

    def test_update_to_expected_wrong_value(self):
        Issue.objects.create(expected=False)
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.patch(reverse('api:issue-detail', kwargs={"pk": 1}), data={"id": 1, "expected": "Str"},
                                     content_type="application/json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()["expected"], ["Must be a valid boolean."])

    def test_try_to_update_read_only_field(self):
        Issue.objects.create(runconfigs_covered_count=100)
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.patch(reverse('api:issue-detail', kwargs={"pk": 1}),
                                     data={"id": 1, "runconfigs_covered_count": 99}, content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["runconfigs_covered_count"], 100)


class UnknownFailureViewSetTests(TestCase):
    def test_retrieve_by_id(self):
        unknown_failure = baker.make(UnknownFailure)
        response = self.client.get(
            reverse("api:unknownfailure-detail", kwargs={"pk": unknown_failure.id}), content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data["id"], unknown_failure.id)
        self.assertEqual(data["test"], unknown_failure.result.test.name)
        self.assertEqual(data["status"], unknown_failure.result.status.name)
        self.assertRaises(KeyError, lambda: data["dmesg"])
        self.assertRaises(KeyError, lambda: data["stdout"])
        self.assertRaises(KeyError, lambda: data["stderr"])
        self.assertEqual(data["testsuite"], unknown_failure.result.ts_run.testsuite.name)
        self.assertEqual(data["runconfig"], {
            "name": unknown_failure.result.ts_run.runconfig.name,
            "tags": list(unknown_failure.result.ts_run.runconfig.tags.values_list('name', flat=True))
        })
        self.assertEqual(data["machine"], {
            "name": unknown_failure.result.ts_run.machine.name,
            "tags": list(unknown_failure.result.ts_run.machine.tags.values_list('name', flat=True))
        })

    def test_retrieve_by_id__extra_fields(self):
        unknown_failure = baker.make(UnknownFailure)
        response = self.client.get(
            reverse("api:unknownfailure-detail", kwargs={"pk": unknown_failure.id}),
            content_type="application/json",
            data={"extra_fields": "stdout"},
        )
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertRaises(KeyError, lambda: data["dmesg"])
        self.assertRaises(KeyError, lambda: data["stderr"])
        self.assertEqual(data["stdout"], unknown_failure.result.stdout)

    def test_list(self):
        unknown_failures = baker.make(UnknownFailure, _quantity=3)
        response = self.client.get(
            reverse("api:unknownfailure-list"),
            content_type="application/json",
        )
        data = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data["count"], 3)
        for unknown_failure in unknown_failures:
            self.assertIn(
                {
                    "id": unknown_failure.id,
                    "test": unknown_failure.result.test.name,
                    "status": unknown_failure.result.status.name,
                    "testsuite": unknown_failure.result.ts_run.testsuite.name,
                    "runconfig": {
                        "name": unknown_failure.result.ts_run.runconfig.name,
                        "tags": list(unknown_failure.result.ts_run.runconfig.tags.values_list('name', flat=True))
                    },
                    "machine": {
                        "name": unknown_failure.result.ts_run.machine.name,
                        "tags": list(unknown_failure.result.ts_run.machine.tags.values_list('name', flat=True))
                    },
                },
                data["results"],
            )


class TestSetTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()
        ts = TestSuite.objects.create(name="ts", description="", url="", public=True)
        Test.objects.create(id=1, name="test1", testsuite=ts, public=True)

    def test_vet_test_without_permission(self):
        self.user = create_user_and_log_in(self.client, admin=False)
        response = self.client.post(reverse('api:test-vet', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 403)

    @patch("django.utils.timezone.now")
    def test_vet_test(self, now_mocked):
        date = datetime.datetime(2021, 1, 1, tzinfo=datetime.timezone.utc)
        now_mocked.return_value = date
        test = baker.make(Test, vetted_on=None)
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.post(reverse('api:test-vet', kwargs={"pk": test.pk}))
        test.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(test.vetted_on, date)

    def test_vet_already_vetted(self):
        date = datetime.datetime(2021, 1, 1, tzinfo=datetime.timezone.utc)
        test = baker.make(Test, vetted_on=date)
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.post(reverse('api:test-vet', kwargs={"pk": test.pk}))
        test.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(test.vetted_on, date)

    def test_suppress_test_without_permission(self):
        self.user = create_user_and_log_in(self.client, admin=False)
        response = self.client.post(reverse('api:test-suppress', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 403)

    def test_suppress_test(self):
        test = baker.make(Test, vetted_on=datetime.datetime(2021, 1, 1))
        self.user = create_user_and_log_in(self.client, admin=True)
        response = self.client.post(reverse('api:test-suppress', kwargs={"pk": test.pk}))
        test.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(test.vetted_on)


class TextStatusViewSetTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()
        self.user = create_user_and_log_in(self.client, admin=True)

    @patch("django.utils.timezone.now")
    def test_vet(self, now_mocked):
        date = datetime.datetime(2021, 1, 1, tzinfo=datetime.timezone.utc)
        now_mocked.return_value = date
        ts = baker.make(TextStatus, vetted_on=None)
        response = self.client.get(reverse('api:textstatus-vet', kwargs={"pk": ts.pk}))
        ts.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(ts.vetted_on, date)

    def test_suppress(self):
        ts = baker.make(TextStatus, vetted_on="2021-01-01")
        response = self.client.get(reverse('api:textstatus-suppress', kwargs={"pk": ts.pk}))
        ts.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(ts.vetted_on)


class metrics_passrate_trend_viewTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()

    def test_basic(self):
        # TODO: Add a fixture that would return useful data here
        response = self.client.get(reverse('api-metrics-passrate-per-runconfig'),
                                   {'query': ""}, format='json')
        self.assertEqual(response.status_code, 200)


class metrics_passrate_viewTests(TestCase):
    def setUp(self):
        self.arf = APIRequestFactory()

    def test_basic(self):
        # TODO: Add a fixture that would return useful data here
        response = self.client.get(reverse('api-metrics-passrate-per-test'),
                                   {'query': ""}, format='json')
        self.assertEqual(response.status_code, 200)
