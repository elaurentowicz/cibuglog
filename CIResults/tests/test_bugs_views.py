from datetime import timedelta
from unittest.mock import patch, MagicMock

from django.conf import settings
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from CIResults.tests.test_views import ViewMixin, UserFiltrableViewMixin, create_user_and_log_in
from CIResults.models import BugTracker, Bug, ReplicationScript
from CIResults.serializers import serialize_bug

import json

# HACK: Massively speed up the login primitive. We don't care about security in tests
settings.PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )


class ReplicationScriptCheckTests(TestCase):

    def setUp(self):
        self.user = create_user_and_log_in(self.client, permissions=['change_replicationscript'])
        self.url = reverse("CIResults-replication-script-check")
        self.bt1 = BugTracker.objects.create(name="Tracker1", project="TEST", tracker_type="bugzilla",
                                             url="http://bar", public=True)
        self.bt2 = BugTracker.objects.create(name="Tracker2", project="TEST", tracker_type="jira",
                                             url="http://bar", public=True)
        self.kwargs = {"script": "def foo(): pass",
                       "source_tracker": "Tracker1",
                       "destination_tracker": "Tracker2"}

    @patch('CIResults.bugs_views.Client')
    @patch('CIResults.bugtrackers.BugTrackerCommon.tracker_check_replication')
    def test_replication_script_check(self, t_rep_mock, c_mock):
        bug = Bug.objects.create(tracker=self.bt1, bug_id='2',
                                 title="You don't win friends with salad!", description="steamed hams")
        ser_bug = serialize_bug(bug)
        exp_resp = {'bugs': [
                {'operation': "create", 'src_bug': ser_bug, 'dest_bug': None}
        ]}

        t_rep_mock.return_value = [{"operation": "create", 'src_bug': ser_bug, 'dest_bug': None}]
        resp = self.client.post(self.url, self.kwargs)
        self.assertEqual(resp.status_code, 200)
        output = json.loads(resp.getvalue().decode())
        self.assertEqual(output, exp_resp)

    @patch('CIResults.bugs_views.Client')
    @patch('CIResults.bugtrackers.BugTrackerCommon.tracker_check_replication')
    def test_replication_script_check_no_resp(self, t_rep_mock, c_mock):
        Bug.objects.create(tracker=self.bt1, bug_id=2,
                           title="You don't win friends with salad!", description="steamed hams")
        exp_resp = {'bugs': []}

        t_rep_mock.return_value = []
        resp = self.client.post(self.url, self.kwargs)
        self.assertEqual(resp.status_code, 200)
        output = json.loads(resp.getvalue().decode())
        self.assertEqual(output, exp_resp)

    @patch('CIResults.bugs_views.Client')
    @patch('CIResults.bugtrackers.BugTrackerCommon.tracker_check_replication')
    def test_replication_script_check_errors(self, t_rep_mock, c_mock):
        class FooError(Exception):
            def __init__(self, return_code, reason):
                self.return_code = return_code
                self.reason = reason

        c_mock.UserFunctionCallError = FooError
        Bug.objects.create(tracker=self.bt1, bug_id=2,
                           title="You don't win friends with salad!", description="steamed hams")

        t_rep_mock.side_effect = FooError("foo", "foo")
        resp = self.client.post(self.url, self.kwargs)
        self.assertEqual(resp.status_code, 500)

        t_rep_mock.side_effect = ValueError()
        resp = self.client.post(self.url, self.kwargs)
        self.assertEqual(resp.status_code, 500)

    @patch('CIResults.bugs_views.Client')
    @patch('CIResults.bugtrackers.BugTrackerCommon.tracker_check_replication')
    def test_replication_script_check_update(self, t_rep_mock, c_mock):
        bug = Bug.objects.create(tracker=self.bt1, bug_id='2',
                                 title="You don't win friends with salad!", description="steamed hams")
        dest_bug = Bug.objects.create(tracker=self.bt2, bug_id='3', parent=bug, title="Something clever",
                                      description="Even more clever")
        ser_bug = serialize_bug(bug)
        ser_dest_bug = serialize_bug(dest_bug)
        exp_resp = {'bugs': [
                {'operation': "update", 'src_bug': ser_bug, 'dest_bug': ser_dest_bug}
        ]}

        t_rep_mock.return_value = [{"operation": "update", 'src_bug': ser_bug, 'dest_bug': ser_dest_bug}]
        resp = self.client.post(self.url, self.kwargs)
        self.assertEqual(resp.status_code, 200)
        output = json.loads(resp.getvalue().decode())
        self.assertEqual(output, exp_resp)

    @patch('CIResults.bugs_views.Client')
    def test_replication_script_check_client_errors(self, c_mock):
        c_mock.get_or_create_instance = MagicMock(side_effect=ValueError())
        resp = self.client.post(self.url, self.kwargs)
        self.assertEqual(resp.status_code, 500)

        c_mock.get_or_create_instance = MagicMock(side_effect=IOError())
        resp = self.client.post(self.url, self.kwargs)
        self.assertEqual(resp.status_code, 500)


class MetricsOpenBugsTests(UserFiltrableViewMixin, TestCase):
    fixtures = ['CIResults/fixtures/bugs']

    # TODO: set the current time to a fixed one, add more comments

    reverse_name = "CIResults-bug-open-list"
    query = "product = 'test'"


class bug_flag_for_updateTests(TestCase):
    def setUp(self):
        tracker = BugTracker.objects.create(name="BugTracker", public=True)
        self.bug = Bug.objects.create(tracker=tracker, bug_id="BUG_1")

    def test_not_being_updated(self):
        response = self.client.post(reverse("CIResults-bug-flag-for-update", kwargs={"pk": self.bug.pk}))
        self.assertEqual(response.status_code, 200)

        delta = timezone.now() - Bug.objects.get(pk=self.bug.id).flagged_as_update_pending_on
        self.assertLess(delta.total_seconds(), 1)

    def test_being_updated(self):
        self.bug.flagged_as_update_pending_on = timezone.now() - timedelta(seconds=10)
        self.bug.save()

        response = self.client.post(reverse("CIResults-bug-flag-for-update", kwargs={"pk": self.bug.pk}))
        self.assertEqual(response.status_code, 409)

        delta = timezone.now() - Bug.objects.get(pk=self.bug.id).flagged_as_update_pending_on
        self.assertGreater(delta.total_seconds(), 1)


class ReplicationScriptTests(ViewMixin, TestCase):
    reverse_name = "CIResults-replication-script-list"


class ReplicationScriptCreateTests(ViewMixin, TestCase):
    reverse_name = "CIResults-replication-script-create"
    permissions_needed = ['add_replicationscript']

    def setUp(self):
        self.db_tracker = BugTracker.objects.create(name="Tracker1", project="TEST", tracker_type="bugzilla",
                                                    url="http://bar", public=True)
        self.rep_tracker = BugTracker.objects.create(name="Tracker2", tracker_type="jira", url="http://foo",
                                                     project="TEST2", public=True)

    def test_anonymous(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(reverse(self.reverse_name)))

    def test_script_create(self):
        create_user_and_log_in(self.client, permissions=self.permissions_needed)
        response = self.client.post(self.url, {'name': "foo", 'enabled': False, 'source_tracker': self.db_tracker.id,
                                               'destination_tracker': self.rep_tracker.id,
                                               'script': "def foo(): pass"})
        self.assertRedirects(response, reverse("CIResults-replication-script-list"))
        self.assertEqual(len(ReplicationScript.objects.all()), 1)


class ReplicationScriptEditTests(ViewMixin, TestCase):
    reverse_name = "CIResults-replication-script-edit"

    def __post(self):
        return self.client.post(self.url, {'name': "BAR", 'script': "def bar(): pass", 'source_tracker': self.bt1.id,
                                           'destination_tracker': self.bt2.id, 'enabled': 'True'})

    def setUp(self):
        self.user = create_user_and_log_in(self.client, permissions=['change_replicationscript'])
        self.bt1 = BugTracker.objects.create(name="Tracker1", project="TEST", tracker_type="bugzilla",
                                             url="http://bar", public=True)
        self.bt2 = BugTracker.objects.create(name="Tracker2", project="TEST", tracker_type="jira",
                                             url="http://bar", public=True)
        self.rp = ReplicationScript.objects.create(name="FOO", script="def foo(): pass",
                                                   source_tracker=self.bt1, destination_tracker=self.bt2, enabled=True,
                                                   created_by=self.user)
        self.create_time = self.rp.created_on
        self.view_kwargs = {'pk': self.rp.pk}

    def test_script_history(self):
        response = self.__post()
        expected = [{'name': "FOO",
                     'created_by': self.user.username,
                     'created_on': self.create_time.isoformat()[:-6]+"Z",
                     'source_tracker': "Tracker1",
                     'destination_tracker': "Tracker2",
                     'script': "def foo(): pass"}]
        rep_script = ReplicationScript.objects.first()
        history = rep_script.script_history
        self.assertRedirects(response, reverse("CIResults-replication-script-list"))
        self.assertEqual(history, json.dumps(expected))

    def test_anonymous(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(self.url))
