import json
from unittest.mock import patch, MagicMock
from rest_framework.test import APIRequestFactory
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from CIResults.models import Test, Machine, RunConfigTag, TestSuite, Build
from CIResults.models import TextStatus, IssueFilter, Issue
from CIResults.models import RunConfig, TestsuiteRun, TestResult, Component
from CIResults.views import IssueFilterView, ResultsCompareView

from datetime import timedelta


# HACK: Massively speed up the login primitive. We don't care about security in tests
settings.PASSWORD_HASHERS = ('django.contrib.auth.hashers.MD5PasswordHasher', )


def create_user_and_log_in(client, admin=False, permissions=[]):
    user = get_user_model().objects.create_user('user', 'user@provider.com', 'pwd',
                                                first_name='First', last_name='Last',
                                                is_superuser=admin)
    perm_objs = []
    for codename in permissions:
        perm_objs.append(Permission.objects.get(codename=codename))
    user.user_permissions.add(*perm_objs)

    client.login(username='user', password='pwd')
    return user


class ViewMixin:
    view_kwargs = {}

    @property
    def url(self):
        return reverse(self.reverse_name, kwargs=self.view_kwargs)

    def test_get__authorized(self):
        if hasattr(self, 'permissions_needed'):
            create_user_and_log_in(self.client, permissions=self.permissions_needed)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_get__unauthorized_access(self):
        if not hasattr(self, 'permissions_needed'):
            self.skipTest("No need for authorization for the class")  # pragma: no cover

        create_user_and_log_in(self.client, permissions=[])

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)


class UserFiltrableViewMixin(ViewMixin):
    def test_invalid_query(self):
        response = self.client.get(self.url + "?query=djzkhjkf")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Filtering error")

    def test_valid_query(self):
        response = self.client.get(self.url + "?query=" + self.query)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "Filtering error")


class IndexTests(ViewMixin, TestCase):
    reverse_name = "CIResults-index"


class IssueListTests(UserFiltrableViewMixin, TestCase):
    reverse_name = "CIResults-issues-list"
    query = "filter_description = 'desc'"


class IssueAddTests(ViewMixin, TestCase):
    reverse_name = "CIResults-issue"
    permissions_needed = ['add_issue']
    view_kwargs = {'action': 'create'}


class IssueEditTests(ViewMixin, TestCase):
    reverse_name = "CIResults-issue"
    permissions_needed = ['change_issue']

    def setUp(self):
        issue = Issue.objects.create()
        self.view_kwargs = {'action': 'edit', 'pk': issue.pk}


class IssueMiscTests(TestCase):
    post_actions = ["archive", "restore", "hide", "show"]

    def setUp(self):
        self.issue = Issue.objects.create()

    def url(self, action):
        return reverse("CIResults-issue", kwargs={'action': action, 'pk': self.issue.pk})

    def test_post__unauthorized_access(self):
        create_user_and_log_in(self.client, permissions=[])
        for action in self.post_actions:
            with self.subTest(action=action):
                response = self.client.post(self.url(action), {})
                self.assertEqual(response.status_code, 403)

    def test_post__authorized_access(self):
        for action in self.post_actions:
            with self.subTest(action=action):
                user = create_user_and_log_in(self.client, permissions=['{}_issue'.format(action)])

                response = self.client.post(self.url(action), {})
                self.assertEqual(response.status_code, 302)
                self.assertEqual(response.url, reverse('CIResults-index'))

                user.delete()

    def test_get_on_post_action(self):
        for action in self.post_actions:
            with self.subTest(action=action):
                self.assertRaises(ValidationError, self.client.get, self.url(action))


class TestTests(ViewMixin, TestCase):
    reverse_name = "CIResults-tests"


class TestMassRenameTests(ViewMixin, TestCase):
    reverse_name = "CIResults-tests-massrename"
    permissions_needed = ['change_test']


class TestRenameTests(ViewMixin, TestCase):
    reverse_name = "CIResults-test-rename"
    permissions_needed = ['change_test']

    def setUp(self):
        testsuite = TestSuite.objects.create(name='ts1', public=True)
        t = Test.objects.create(name='test', testsuite=testsuite, public=True)
        self.view_kwargs = {'pk': t.pk}


class MachineTests(ViewMixin, TestCase):
    reverse_name = "CIResults-machines"


class TestResultListViewTests(UserFiltrableViewMixin, TestCase):
    reverse_name = "CIResults-results"
    query = "test_name = 'test'"


class KnownFailureListViewTests(UserFiltrableViewMixin, TestCase):
    reverse_name = "CIResults-knownfailures"
    query = "test_name = 'test'"


class ResultsCompareTests(ViewMixin, TestCase):
    reverse_name = "CIResults-compare"

    def test_urlify(self):
        view = ResultsCompareView()
        string = "Hello http://gitlab.freedesktop.org, https://x.org"
        self.assertEqual(view.urlify(string),
                         "Hello <http://gitlab.freedesktop.org>, <https://x.org>")

    def test_invalid_runconfig(self):
        response = self.client.get(self.url + "?from=RUNCONFIG1&to=RUNCONFIG2")
        self.assertEqual(response.status_code, 200)

    @patch('CIResults.models.RunConfig.objects.filter')
    def test_valid_runconfig(self, filter_mocked):
        filter_mocked.return_value.first.return_value.compare.return_value.text = "Test"
        response = self.client.get(self.url + "?from=RUNCONFIG1&to=RUNCONFIG2")
        self.assertEqual(response.status_code, 200)


class MassVettingMixin:
    def test_get_request_should_fail(self):
        create_user_and_log_in(self.client, permissions=[self.needed_permission])
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 405)

    def test_normal_query__without_privileges(self):
        create_user_and_log_in(self.client)
        response = self.client.post(self.url, {})
        self.assertEqual(response.status_code, 403)

    def test_normal_query__with_privileges(self):
        create_user_and_log_in(self.client, permissions=[self.needed_permission])

        with patch('CIResults.models.{}.objects.filter'.format(self.klass),
                   return_value=[MagicMock(name='object 1'),
                                 MagicMock(name='object 2'),
                                 MagicMock(name='object 3')]) as filter_mocked:
            response = self.client.post(self.url, {'1': 'on', '2': 'on', '3': 'on', 'gfdgdfg': 'invalid'})
            self.assertEqual(response.status_code, 302)

            # Check that all the machines were requested from the DB
            filter_mocked.assert_called_once_with(pk__in=set([1, 2, 3]))

            # Check that all machine returned were vetted
            for obj in filter_mocked.return_value:
                obj.vet.assert_called_once_with()


class MachineMassVettingTests(MassVettingMixin, TestCase):
    def setUp(self):
        self.url = reverse('CIResults-machine-mass-vetting')
        self.klass = "Machine"
        self.needed_permission = 'vet_machine'


class TestMassVettingTests(MassVettingMixin, TestCase):
    def setUp(self):
        self.url = reverse('CIResults-test-mass-vetting')
        self.klass = "Test"
        self.needed_permission = 'vet_test'


class TextStatustMassVettingTests(MassVettingMixin, TestCase):
    def setUp(self):
        self.url = reverse('CIResults-textstatus-mass-vetting')
        self.klass = "TextStatus"
        self.needed_permission = 'vet_textstatus'


class IssueDetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-issue-detail"

    def setUp(self):
        issue = Issue.objects.create(filer='me@me.org', expected=True)
        self.view_kwargs = {'pk': issue.pk}


class IFADetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-ifa-detail"

    def setUp(self):
        issue = Issue.objects.create(filer='me@me.org', expected=True)
        f = IssueFilter.objects.create(description='filter')

        user = get_user_model().objects.create_user('user')
        issue.set_filters([f], user)
        pk = f.issuefilterassociated_set.first().pk
        self.view_kwargs = {'pk': pk}


class IssueFilterViewTests(TestCase):
    def setUp(self):
        self.issueFilterView = IssueFilterView()

    def test_parse_filter_from_params(self):
        filter = self.issueFilterView.__parse_filter_from_params__(
            {"description": "Description", "stdout_regex": "stdout regex"}
        )
        self.assertEqual(filter.description, "Description")
        self.assertEqual(filter.stdout_regex, "stdout regex")

    def test_convert_to_user_query(self):
        arf = APIRequestFactory()
        request = arf.post("/", {"stderr_regex": "stderr regex"}, format="json")
        user_query = self.issueFilterView.__convert_to_user_query__(request)
        self.assertEqual(json.loads(user_query.content)["userQuery"], "stderr ~= 'stderr regex'")


class MachineDetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-machine-detail"

    def setUp(self):
        machine = Machine.objects.create(name='Machine1', public=True)
        self.view_kwargs = {'pk': machine.pk}


class TestSuiteDetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-testsuite-detail"

    def setUp(self):
        ts = TestSuite.objects.create(name='TestSuite', public=True)
        self.view_kwargs = {'pk': ts.pk}


class TestDetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-test-detail"

    def setUp(self):
        ts = TestSuite.objects.create(name='TestSuite', public=True)
        test = Test.objects.create(name='test1', testsuite=ts, public=True)
        self.view_kwargs = {'pk': test.pk}


class TextStatusDetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-textstatus-detail"

    def setUp(self):
        ts = TestSuite.objects.create(name='TestSuite', public=True)
        txt_stat = TextStatus.objects.create(name='status1', testsuite=ts)
        self.view_kwargs = {'pk': txt_stat.pk}


class TestResultDetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-testresult-detail"

    def setUp(self):
        ts = TestSuite.objects.create(name='TestSuite', public=True)
        machine = Machine.objects.create(name='Machine1', public=True)
        runconfig = RunConfig.objects.create(name='runconfig', temporary=True)
        ts_run = TestsuiteRun.objects.create(testsuite=ts, machine=machine, runconfig=runconfig,
                                             run_id=0, start=timezone.now(), duration=timedelta(hours=3))

        test = Test.objects.create(name='test1', testsuite=ts, public=True)
        status = TextStatus.objects.create(name='status1', testsuite=ts)
        tr = TestResult.objects.create(test=test, status=status, ts_run=ts_run,
                                       start=timezone.now(), duration=timedelta(seconds=5))
        self.view_kwargs = {'pk': tr.pk}


class RunConfigDetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-runcfg-detail"

    def setUp(self):
        cfg = RunConfig.objects.create(name='runconfig', temporary=True)
        self.view_kwargs = {'pk': cfg.pk}


class RunConfigTagDetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-runcfgtag-detail"

    def setUp(self):
        cfg = RunConfigTag.objects.create(name='tag', public=True)
        self.view_kwargs = {'pk': cfg.pk}


class BuildDetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-build-detail"

    def setUp(self):
        component = Component.objects.create(name='component', public=True)
        build = Build.objects.create(name='build1', component=component)
        self.view_kwargs = {'pk': build.pk}


class ComponentDetailTests(ViewMixin, TestCase):
    reverse_name = "CIResults-component-detail"

    def setUp(self):
        comp = Component.objects.create(name='runconfig', public=True)
        self.view_kwargs = {'pk': comp.pk}
