/*
 * Adds a row to the tbody of a table identified by an id (#mytable) that has
 * an attribute data-row-next-id set,.
 *
 * WARNING: if data-row-next-id is set to 0, then the tbody is cleared before
 *          adding anything. This is useful to add placeholders.
 */
function table_add_row(table_id) {
    // get an id for the new row, and increment the storage
    var id = $(table_id).data("row-next-id")
    $(table_id).data("row-next-id", id + 1)

    // Remove the current content (placeholder) if the the id is 0
    if (id == 0)
        $(table_id+" tbody").html("");

    // Create a new row
    row = $("<tr></tr>")
    row.data('id', id)

    // Move the row to the right location
    $(table_id+" tbody").append(row)

    return row
}

function table_add_global_filtering() {
    $("table.global-filtrable").each(function(table_iter) {
        // Create prefix to prevent name conflicts
        const prefix= $(this).attr('id') ? $(this).attr('id') + "_" : ''
        const url = new URL(window.location.href);
        const urlParams = new URLSearchParams(window.location.search)
        const $form = $(`<form method="get" id="form-${table_iter}"></form>`)
        $form.on("submit", (event) => {
            event.preventDefault();
            const formData = new FormData($(`#form-${table_iter}`)[0]);
            for (const [key, value] of formData.entries()) {
                urlParams.set(key, value);
            }
            const newUrl = `${url.origin}${url.pathname}?${urlParams.toString()}`;
            window.location.href = newUrl;
        });
        $(this).wrap($form)
        $(this).find("thead").append("<input type='submit' hidden/>")
        $(this).find("thead tr th").each(function( i ) {
            const className = $(this).attr('class')
            // Skip excluded columns
            if (className && className.match(/\bfiltrable-nofilter\b/)) {
                return
            }

            let name = prefix + $(this).text().toLowerCase().replace(/ /g, '_').replace(/\(|\)/g, '')
            if ($(this).attr('data-filtrable-name')) {
                name = prefix + ($(this).attr('data-filtrable-name'))
            }
            const dataType = (className && className.match(/\bfiltrable-date\b/)) ? 'date': 'text'

            // Create input fields
            $(this).append(`<div class='input-group'><input class='form-control form-control-sm' type='${dataType}' placeholder='Filter...' id='input-${i}' name='${name}' value='${urlParams.get(name) || ''}'></div>`);
        });
    });
}

function table_add_filtering() {
    $("table.filtrable").each(function() {
        var table = this;
        $(this).find("thead tr th").each(function( i ) {
            // Create input fields
            $(this).append("<br><div class='input-group'><input class='form-control form-control-sm' type='text' placeholder='Filter...' id='input-" + i + "'></div>");

            $("input", this).on("change", function(){
                $(table).find("tbody tr").each(function() {
                    var bool = true;
                    $(this).find("td").each(function( j ){
                        if($(this).text().match($(table).find("#input-" + j).val()) === null) {
                            bool = false;
                        }
                    });
                    $(this).toggle(bool);
                });
            });
        });
    });
}


function handleRemoveRow() {
    $('[data-remove-row]').click(function() {
        $(this).closest("tr").remove();
    });
}

function handleEditFilter() {
    $("[data-edit-filter]").click(function(click){
        const filterId = click.target.parentNode.closest("tr").getElementsByTagName("td")[0].innerHTML
        $(window).trigger('issue-edit-filter', filterId);
    });
}

function arrayToInt(array) {
    for (var i in array)
        array[i] = parseInt(array[i], 10);
    return array;
}

function do_background_post_request(url, attributes) {
    var formData = new FormData();
    formData.append("csrfmiddlewaretoken", $("[name=csrfmiddlewaretoken]").val());
    for(var key in attributes) {
        formData.append(key, attributes[key]);
    }

    var request = new XMLHttpRequest();
    request.open("POST", url);
    request.send(formData);

    return request
}

function handleConfirmations() {
    var current = null;

    /* Find all the href with data-confirmation */
    $('a[data-confirmation]').click(function() {
        current = this
        showModal({title: "Attention", content: $(this).data("confirmation"), buttons: [{label: 'Cancel'}, {label: 'Yes', style: 'primary', callback: () => {
                        req = do_background_post_request(current.href, {});

            if (req.status == 200) {
                if ($(current).data('confirmation-reload') !== undefined) {
                    location.reload();
                }
            } else if ($(current).data('confirmation-msg-on-error') !== undefined) {
                alert($(current).data("confirmation-msg-on-error"), "Error");
            }
            current = null;
        }}]})
        return false;
    });

    /* For forms */
    $('button[data-submit-confirm]').click(function() {
        current_form = $(this).closest("form")
        checked = $(current_form).find('input[type="checkbox"]:checked').length

        msg = $(this).data("submit-confirm")
        msg = msg.replace("{checked}", checked)

        showModal({title: "Attention", content: msg, buttons: [{label: 'Cancel'}, {label: 'Yes', style: 'primary', callback: () => {$(current_form).submit()}}]})
        return false;
    });
}

function handleInforms() {
    /* Find all the href with data-confirmation */
    $('a[data-inform]').click(function() {
        showModal({title: "Information", content: $(this).data("inform"), size: 'lg'})
        return false;
    });

    /* Find all the href with data-confirmation */
    $('a[data-inform-target]').click(function() {
        showModal({title: "Information", content: $("#" + $(this).data("inform-target")).html(), size: 'lg'})
        return false;
    });
}

function handleSelectAll() {
    /* Add a checkbox */
    $('[data-select-name]').each(function(){
        $(this).prepend('<span class="select-item"><input type="checkbox" name="' + $(this).data("select-name") + '"/></span>');
    });

    $('a[data-select-all]').click(function() {
        $("#" + $(this).data("select-all")).find("input").prop('checked', true);
        return false;
    });

    $('a[data-unselect-all]').click(function() {
        $("#" + $(this).data("unselect-all")).find("input").prop('checked', false);
        return false;
    });
}

function handleDualListBox() {
    const selects = document.querySelectorAll("[data-dual-list-box]")
    selects.forEach(select => {
      initDualListbox(select)
    })
}

function setAccountIsDev(account_id, is_dev) {
    var xhr = new XMLHttpRequest();

    // FIXME: Find a way not to hardcode the URL here...
    xhr.open('PATCH', "../api/bugtrackeraccount/" + account_id + "/");
    xhr.setRequestHeader('X-CSRFToken', '{{ csrf_token }}');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function() {
        if (xhr.status === 200) {
            location.reload();
        }
    };
    xhr.send(JSON.stringify({"is_developer": is_dev}));
}

function handleAccountEdit() {
    /* Find all the href with data-confirmation */
    $('a[data-account-edit]').click(function() {
        var account_id = $(this).data("account-edit")

        var options = {
            content: 'What role should this account have?',
            title: "Account role selection",
            size: 'sm',
            buttons: [
                {label: 'Cancel', style: 'default', callback: function() {
                    // Nothing to do
                }
                },
                {label: 'User', style: 'success', click: function() {
                        setAccountIsDev(account_id, false);
                    }
                },
                {label: 'Dev', style: 'primary', click: function() {
                        setAccountIsDev(account_id, true);
                    }
                },
            ],
        };

        showModal(options)
        return false;
    });
}

function formDisableOnSubmit() {
    $('form[data-disable-on-submit').submit(function() {
        txt = $(this).data('disable-on-submit')
        $("[type='submit']", this)
            .html(txt)
            .attr('disabled', 'disabled');

        return true;
    });
}

function handleSetAsQuery() {
    var queryInput = $('#userFilteringQuery');
    $('a[data-set-as-query]').click(function() {
        queryInput.attr('value', $(this).data("set-as-query"))
    })

    var datalist = $('#' + queryInput.attr('list'));
    var queryKeywords = [];
    datalist.children('option').each(function() {
        queryKeywords.push($(this).text());
    });
    queryInput.on('input', function() {
        var parts = $(this).val().split(' ');
        var lastWord = parts[parts.length - 1];
        // If we're trying to autocomplete an empty word, don't suggest
        // anything. Wait for the user to type at least one letter to avoid
        // confusing suggestions.
        if (lastWord == '') {
            datalist.empty();
            return;
        }
        var prefix = parts.slice(0, -1).join(' ');
        var options = [];
        for (var i = 0; i < queryKeywords.length; i++) {
            var keyword = queryKeywords[i];
            if (keyword.indexOf(lastWord) == 0) {
                options.push($('<option></option>').text(prefix+' '+keyword));
            }
        }
        datalist.html(options);
    }).trigger('input');
}

function str_to_csv_value(string) {
    return '"' + string.replace('"', '""').trim() + '"'
}

const copyUrlParams = {}

function handleHideableColumns() {
    $('div.column-selector').each(function() {
        var selector = $(this);
        var class_name = selector.data('table-class');

        // Get all the columns and check for consistancy
        var columns = [];
        $('table.' + class_name).each(function() {
            var table_columns = $(this).find('th').map(function() {
                return {
                    name: $(this).text(),
                    visibility: $(this).data('visibility')
                };
            });

            if (columns.length == 0) {
                columns = table_columns;
            } else {
                var inconsistent = false;
                if (columns.length == table_columns.length) {
                    for (var i = 0; i < columns.length; i++) {
                        if (columns[i].text != table_columns[i].text ||
                            columns[i].visibility != table_columns[i].visibility)
                            inconsistent = true;
                    }
                } else
                    inconsistent = true;

                if (inconsistent)
                    console.warn('Table with class "' + class_name + '" have inconsistent header');
            }
        });

        // Read the user preferences
        var storage_id = 'table_columns__' + class_name;
        var preferences = JSON.parse(localStorage.getItem(storage_id));
        if (preferences === null)
            preferences = {}

        // Set the columns in the selector
        var html = '<label for="userFilteringQuery">Show columns</label>\n';
        for (var i = 0; i < columns.length; i++) {
            var col_show = preferences[columns[i].name];

            var attrs = '';
            if (columns[i].visibility == 'always')
                attrs += 'checked disabled ';
            else if (col_show || (typeof col_show === 'undefined' && columns[i].visibility == 'show'))
                attrs += 'checked ';

            html += '<div class="checkbox"><label><input type="checkbox" data-td-offset="' + (i + 1) + '"' + attrs + '> ' + columns[i].name + '</label></div>\n';
        }

        var download_as = selector.data('table-downloadable-as');
        if (download_as != null) {
            html += '<div class="checkbox"><a class="" href="#" download='+download_as+' title="Download as CSV" data-table-as-csv-class='+class_name+'><i class="bi bi-file-arrow-down"></i> CSV</a></div>'

            $(this).html(html);

            selector.find('a[download]').click(function() {
                var headers = [];
                $('table.' + class_name).find('th').each(function() {
                  if ($(this).is(':visible')) {
                    headers.push(str_to_csv_value($(this).text()))
                  }
                })

                var rows = [];
                $('table.' + class_name).find('tr').each(function() {
                    var cols = [];
                    $(this).find('td').each(function() {
                        if ($(this).is(':visible')) {
                          cols.push(str_to_csv_value($(this).text().trim()));
                        }
                    });

                    if (cols.length == headers.length)
                        rows.push(cols);
                });

                // Write the data as CSV
                var csv = headers.join() + "\n";
                for (var i = 0; i < rows.length; i++) {
                    csv += rows[i].join() + "\n"
                }

                var blob = new Blob([csv], {type:"text/csv;charset=utf-8"})
                var fname = $(this).attr("download") || "table.csv"
                saveAs(blob, fname)

                return false;
            })
        } else {
            $(this).html(html);
        }

        // Now make sure we hide the columns we don't want, and react to changes
        const show_hide = (checkbox, setup=false) => {
            var col_to_hide = $(checkbox).data('td-offset');

            objs = $('table.' + class_name).find("td:nth-of-type(" + col_to_hide + "), th:nth-of-type(" + col_to_hide + ")")

            var column = $.trim($(checkbox).parent().text());
            preferences[column] = checkbox.checked;
            const urlColumnName = `col_${column}_visibility`
            const url = new URL(document.location)
            if (setup && url.searchParams.get(urlColumnName) == 'show')
                $(checkbox).prop('checked', true)
            else if (setup && url.searchParams.get(urlColumnName) == 'hide')
                $(checkbox).prop('checked', false)

            if(checkbox.checked) {
                objs.show()
                copyUrlParams[urlColumnName] = "show"
            } else {
                objs.hide()
                copyUrlParams[urlColumnName] = "hide"
            }
            window.history.replaceState(null, null, url.toString());

            // Store the new preferences
            localStorage.setItem(storage_id, JSON.stringify(preferences));
        };
        selector.find('input').each(function() {show_hide(this, true)});
        selector.find('input').change(function() {show_hide(this)});
    })
}

function handleHelpTexts() {
    $('span.help_text').each(function() {
        $(this).html($(this).html() + ' <i class="bi bi-question-circle"></i>');
        $(this).tooltip()
    })
}

function handleAutoScrollWithHash() {
    if (window.location.hash != '') {
        const name = window.location.hash.slice(1);
        const element = document.getElementById(name)
        if (element.classList.contains('collapse')) {
            element.classList.add('show')
        }
        element.scrollIntoView()
    }
}

function addLinkButtonsToCards() {
    $('.card').each(function() {
        const bodyID = $(this).find('.card-body').attr('id')
        if (!bodyID) return
        const btn = $(`<a href=#${bodyID} class="to-show float-right btn btn-sm btn-outline-light ms-2" data-bs-toggle="tooltip"  data-bs-title="Copy link"><i class="bi bi-link-45deg"></i></a>`)
        btn.click(function(event) {
            event.preventDefault()
            const searchParams = new URLSearchParams({...copyUrlParams, ...new URLSearchParams(window.location.search).entries()})
            navigator.clipboard.writeText(`${window.location.href.split('#')[0]}?${searchParams.toString()}${$(this).attr("href")}`)
        })
        $(this).find('.card-header').append(btn)
        $(this).find('.card-header').addClass('to-hover')
  })
}

function activateTooltips() {
  const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
  const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
}

const sendPostActionRequestAndRefresh = async (url) => {
  const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': csrftoken,
    },
  });
  if (response.ok) {
    location.reload();
  } else {
    alert(`Failed to send request: ${response.status} ${response.statusText}`);
  }
}

/* Handle all the start-up scripts */
$(document).ready(function() {
    handleRemoveRow();

    handleConfirmations();

    handleInforms();

    handleSelectAll();

    handleDualListBox();

    handleAccountEdit();

    formDisableOnSubmit();

    handleSetAsQuery();

    handleHideableColumns();

    handleHelpTexts();

    table_add_filtering();

    table_add_global_filtering();

    addLinkButtonsToCards();

    handleAutoScrollWithHash();

    activateTooltips();
})


