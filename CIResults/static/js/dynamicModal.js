var modalWrap = null;

const showModal = ({title, content, size = null, buttons=[{label: "Close", callback: null}]}) => {
  if (modalWrap !== null) {
    modalWrap.remove();
  }

  const generateButton = (button) => {
    return '<button type="button" class="btn btn-' + (button.style ? button.style : 'secondary') + '"data-bs-dismiss="modal">' + button.label + '</button>'
  }

  modalWrap = document.createElement('div');
  modalWrap.innerHTML = `
    <div class="modal fade" tabindex="-1">
      <div class="modal-dialog${size ? ' modal-' + size : ''}">
        <div class="modal-content">
          <div class="modal-header bg-body">
            <h5 class="modal-title">${title}</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <p>${content}</p>
          </div>
          <div class="modal-footer bg-body">
            ${buttons.map(button => generateButton(button)).join('')}
          </div>
        </div>
      </div>
    </div>
  `;
  const htmlButtons = modalWrap.querySelectorAll('.btn')
  for (let i = 0; i < buttons.length; i++) {
    htmlButtons[i].onclick = buttons[i].callback
  }
  document.body.append(modalWrap);

  var modal = new bootstrap.Modal(modalWrap.querySelector('.modal'));
  modal.show();
}