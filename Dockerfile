FROM python:3.12

WORKDIR /app

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get clean && apt-get update && apt-get -y install build-essential libcap-dev libseccomp-dev libgraphviz-dev
RUN pip install uwsgi

COPY requirements.txt ./
RUN pip install --no-cache-dir -r /app/requirements.txt

COPY . .

ARG VERSION=""
LABEL VERSION=${VERSION}
ENV CIBUGLOG_VERSION=${VERSION}
CMD uwsgi /app/uwsgi-docker.ini
