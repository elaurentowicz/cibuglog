"""cibuglog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import path, re_path
from rest_framework import routers
from django.contrib import admin
from django.conf import settings
from django.views.generic.base import RedirectView
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from CIResults import views, rest_views, metrics_views, bugs_views
from CIResults.models import Test, Machine, TextStatus

router = routers.DefaultRouter()
router.register(r'build', rest_views.BuildViewSet)
router.register(r'bugtracker', rest_views.BugTrackerViewSet)
router.register(r'component', rest_views.ComponentViewSet)
router.register(r'issue', rest_views.IssueViewSet)
router.register(r'issuefilter', rest_views.IssueFilterViewSet)
router.register(r'runconfig', rest_views.RunConfigViewSet)
router.register(r'test', rest_views.TestSet)
router.register(r'textstatus', rest_views.TextStatusViewSet)
router.register(r'bugtrackeraccount', rest_views.BugTrackerAccountViewSet) # WARNING: Hard-coded URL in helpers.js
router.register(r'shortener', rest_views.ShortenerViewSet)
router.register(r'machine', rest_views.MachineViewSet)
router.register(r'unknownfailure', rest_views.UnknownFailureViewSet)


base_patterns = [
    re_path(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^accounts/', include('allauth.urls')),
    path('api/schema.yaml', SpectacularAPIView.as_view(), name='schema'),
    path('api/schema', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),

    re_path(r'^api/metrics/passrate$', rest_views.metrics_passrate_per_runconfig_view, name="api-metrics-passrate"),  # NOTE: Kept for historical reasons
    re_path(r'^api/metrics/passrate_per_runconfig$', rest_views.metrics_passrate_per_runconfig_view, name="api-metrics-passrate-per-runconfig"),
    re_path(r'^api/metrics/passrate_per_test$', rest_views.metrics_passrate_per_test_view, name="api-metrics-passrate-per-test"),
    re_path(r'^api/metrics$', metrics_views.api_metrics),  # WARNING: counts private tests, machines and issues
    re_path(r'^api/bugtracker/(?P<tracker>[^/]+)/bug/(?P<bug_id>[^/]+)', rest_views.BugViewSet.as_view(), name="api-bugtracker-get-bug"),
    re_path(r'^api/', include((router.urls, "CIResults"), namespace='api')),

    re_path(r'^$', views.index, name='CIResults-index'),

    # ReplicationScript
    path('bug/replication/', bugs_views.ReplicationScriptListView.as_view(),
         name='CIResults-replication-script-list'),
    path('bug/replication/create', bugs_views.ReplicationScriptCreateView.as_view(),
         name='CIResults-replication-script-create'),
    path('bug/replication/dryrun', bugs_views.replication_script_check,
         name='CIResults-replication-script-check'),
    path('bug/replication/<pk>', bugs_views.ReplicationScriptEditView.as_view(),
         name='CIResults-replication-script-edit'),

    # Issues
    re_path(r'^issue/$', views.IssueListView.as_view(), name='CIResults-issues-list'),
    re_path(r'^issue/(?P<action>(create))', views.IssueView.as_view(), name='CIResults-issue'),
    re_path(r'^issue/(?P<pk>[0-9]+)(/|/history)?$', views.IssueDetailView.as_view(), name='CIResults-issue-detail'),
    re_path(r'^issue/(?P<pk>[0-9]+)/(?P<action>(edit|archive|restore|show|hide))$', views.IssueView.as_view(), name='CIResults-issue'),
    re_path(r'^issuefilterassoc/(?P<pk>[0-9]+)(/|/history)?$', views.IFADetailView.as_view(), name='CIResults-ifa-detail'),
    re_path(r'^issuefilter/(?P<action>(stats|convert-to-user-query))$', views.IssueFilterView.as_view(), name='CIResults-issuefilter'),

    # Tests
    re_path(r'^test/$', views.TestListView.as_view(), name="CIResults-tests"),
    re_path(r'^test/(?P<pk>[0-9]+)(/|/history)?$', views.TestDetailView.as_view(), name='CIResults-test-detail'),
    re_path(r'^test/(?P<pk>[0-9]+)/edit$', views.TestEditView.as_view(), name='CIResults-test-edit'),
    re_path(r'^test/(?P<pk>[0-9]+)/rename$', views.TestRenameView.as_view(), name='CIResults-test-rename'),
    re_path(r'^test/mass-vetting$', views.TestMassVettingView.as_view(), name='CIResults-test-mass-vetting'),
    re_path(r'^tests/mass-rename$', views.TestMassRenameView.as_view(), name="CIResults-tests-massrename"),

    # Machines
    re_path(r'^machine/$', views.MachineListView.as_view(), name="CIResults-machines"),
    re_path(r'^machine/(?P<pk>[0-9]+)/edit$', views.MachineEditView.as_view(), name='CIResults-machine-edit'),
    re_path(r'^machine/(?P<pk>[0-9]+)(/|/history)?$', views.MachineDetailView.as_view(), name='CIResults-machine-detail'),
    re_path(r'^machine/mass-vetting$', views.MachineMassVettingView.as_view(), name='CIResults-machine-mass-vetting'),

    # Results
    re_path(r'^results/all', views.TestResultListView.as_view(), name="CIResults-results"),
    re_path(r'^results/knownfailures$', views.KnownFailureListView.as_view(), name="CIResults-knownfailures"),
    re_path(r'^results/compare', views.ResultsCompareView.as_view(), name="CIResults-compare"),

    # Bugs
    re_path(r'^bug/(?P<pk>[0-9]+)/flag-for-update', bugs_views.bug_flag_for_update, name="CIResults-bug-flag-for-update"),
    re_path(r'^bug/open_bugs$', bugs_views.open_bugs, name="CIResults-bug-open-list"),


    # Histories
    re_path(r'^testsuite/(?P<pk>[0-9]+)(/|/history)?$', views.TestSuiteDetailView.as_view(), name='CIResults-testsuite-detail'),

    re_path(r'^runcfg/(?P<pk>[0-9]+)(/|/history)?$', views.RunConfigDetailView.as_view(), name='CIResults-runcfg-detail'),
    re_path(r'^component/(?P<pk>[0-9]+)(/|/history)?$', views.ComponentDetailView.as_view(), name='CIResults-component-detail'),
    re_path(r'^build/(?P<pk>[0-9]+)(/|/history)?$', views.BuildDetailView.as_view(), name='CIResults-build-detail'),
    re_path(r'^runcfgtag/(?P<pk>[0-9]+)(/|/history)?$', views.RunConfigTagDetailView.as_view(), name='CIResults-runcfgtag-detail'),
    re_path(r'^testresult/(?P<pk>[0-9]+)(/|/history)?$', views.TestResultDetailView.as_view(), name='CIResults-testresult-detail'),

    # Textstatuses
    re_path(r'^textstatus/mass-vetting$', views.TextStatusMassVettingView.as_view(), name='CIResults-textstatus-mass-vetting'),
    re_path(r'^textstatus/(?P<pk>[0-9]+)(/|/history)?$', views.TextStatusDetailView.as_view(), name='CIResults-textstatus-detail'),

    # Metrics
    re_path(r'^metrics/issues', metrics_views.metrics_issues, name='CIResults-metrics-issues'),
    re_path(r'^metrics/bugs', metrics_views.metrics_bugs, name='CIResults-metrics-bugs'),
    re_path(r'^metrics/comments', metrics_views.metrics_comments, name="CIResults-metrics-comments"),
    re_path(r'^metrics/open_bugs$', RedirectView.as_view(pattern_name='CIResults-bug-open-list'), name="CIResults-metrics-open-bugs"),
    re_path(r'^metrics/passrate_per_runconfig$', metrics_views.metrics_passrate_per_runconfig, name='CIResults-metrics-passrate-per-runconfig'),
    re_path(r'^metrics/passrate_per_test$', metrics_views.metrics_passrate_per_test, name='CIResults-metrics-passrate-per-test'),
    re_path(r'^metrics/passrate$', metrics_views.metrics_passrate_per_runconfig, name='CIResults-metrics-passrate'),  # NOTE: Kept for historical reasons
    re_path(r'^metrics/runtime', metrics_views.metrics_runtime_history, name='CIResults-metrics-runtime'),
]

if settings.DEBUG:
    import debug_toolbar
    base_patterns = [
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
    ] + base_patterns

if settings.URL_PREFIX:
    urlpatterns = [re_path('^{}/'.format(settings.URL_PREFIX), include(base_patterns))]
else:
    urlpatterns = base_patterns
