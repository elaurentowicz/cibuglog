#!/usr/bin/env python3

from multiprocessing import Value
from multiprocessing.dummy import Pool
from datetime import timedelta
from django.utils import timezone

import django
import argparse
import traceback

django.setup()

from CIResults.models import BugTracker, Bug, BugComment  # noqa


def poll_bug(bug_mapped):
    try:
        bt = bug_mapped[0]
        bug = bug_mapped[1]
        if bug.id:
            bug.refresh_from_db()
        bt.poll(bug, force_polling_comments=args.force_polling_comments)
        bug.save()
    except Exception:
        traceback.print_exc()

    with polled.get_lock():
        polled.value += 1
        print("{}/{}: polled {}".format(polled.value, len(bug_map), bug.short_name))


# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("-f", "--force-polling-comments", action="store_true",
                    help="Re-download all the comments associated to bugs")
parser.add_argument("-r", '--remove', default=0, type=int,
                    help="Number of days a bug needs to have been closed before being removed from the database "
                         "(bugs referenced in an issue are not deleted). Use 0 to never remove any bug. Default: 180")
parser.add_argument("-u", '--poll-unreplicated-bugs', action="store_true",
                    help="Poll all unreplicated bugs")
parser.add_argument("-j", '--jobs', default=10, type=int,
                    help="How many parallel jobs should be created to poll the bugs. Default: 10")
args = parser.parse_args()

# Get the full list of bugs that are followed
bug_map = set()
for bt in BugTracker.objects.all():
    try:
        if args.poll_unreplicated_bugs:
            followed_bugs = bt.unreplicated_bugs()
            print("{}: Found {} bugs to replicate".format(bt, len(followed_bugs)))
        else:
            followed_bugs = bt.updated_bugs()
            print("{}: Found {} bugs to follow".format(bt, len(followed_bugs)))

        # add to the list of bugs to poll
        bt_mapped = set(map(lambda x: (bt, x), followed_bugs))
        bug_map.update(bt_mapped)
    except Exception:
        traceback.print_exc()
print()

# Update when the tracker was last polled
# NOTE: This is done before starting polling to make sure bugs that may have been updated
# while polling aren't ignored next polling run
for bt in BugTracker.objects.all():
    bt.polled = bt.tracker_time
    bt.save()

# Start polling in parallel (10 threads) to speed up the polling
print("Polling {} outdated bugs that have been updated since last polling)".format(len(bug_map)))
if len(bug_map) > 0:
    polled = Value('i', 0)
    with Pool(processes=args.jobs) as pool:
        pool.map(poll_bug, bug_map)
else:
    print("Found no bugs to poll. Did you set BugTracker.components_followed_since?")

# Look for all the bugs that are closed and not referenced by any issue
if args.remove > 0:
    deleted_count = 0
    remove_threshold = timezone.now() - timedelta(days=args.remove)
    for bug in Bug.objects.filter(issue=None, closed__lt=remove_threshold).prefetch_related('tracker'):
        if not bug.is_open:
            try:
                bug.delete()
                deleted_count += 1
            except Exception as e:
                print(e)
    print("\nFound {} bugs to be deleted".format(deleted_count))
